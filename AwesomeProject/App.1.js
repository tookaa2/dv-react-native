/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, ScrollView } from "react-native";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "gray", display: "flex" }}>
        <View style={{ flex: 1, backgroundColor: "gray",justifyContent:'center',alignItems:'center'}}>
        <View style={{width:200,height:200,borderRadius:100, backgroundColor:'white',alignContent:'center',justifyContent:'center'}}>
        <Text style={{color:'black',fontSize:25,alignSelf:'center'}}>Image</Text>
        </View>
         
        </View>
        <View style={{ flex: 1, backgroundColor: "gray" }}>
        <View style={{marginHorizontal:20,backgroundColor:'white',height:60,justifyContent:'center'}}>
        <Text style={{fontSize:30,alignSelf:'center'}}>TextInput</Text>
        </View>
        <View style={{marginHorizontal:20,backgroundColor:'white',height:60,marginTop:15,justifyContent:'center'}}>
        <Text style={{fontSize:30,alignSelf:'center'}}>TextInput</Text>
        </View>
        <View style={{marginHorizontal:20,backgroundColor:'black',height:60,position:'absolute',bottom:50,left:0,right:0,justifyContent:'center'}}>
        <Text style={{fontSize:30,color:'white',alignSelf:'center'}}>TextInput</Text>
        </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "auto",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
    display: "flex"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
