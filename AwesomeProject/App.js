/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, ScrollView } from "react-native";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "gray", display: "flex" }}>
        <View
          style={{
            alignSelf: "stretch",
            height: 65,
            backgroundColor: "lime",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "row",
            borderColor: "white",
            borderBottomWidth: 3
          }}
        >
          <View
            style={{ width: 65, backgroundColor: "lime", alignSelf: "stretch" }}
          >
            <Text>icon</Text>
          </View>
          <View
            style={{
              flex: 1,
              alignContent: "center",
              justifyContent: "center",
              borderColor: "white",
              borderLeftWidth: 3,
              borderRightWidth: 3,
              height: 65
            }}
          >
            <Text style={{ alignSelf: "center", color: "white", fontSize: 25 }}>
              Text
            </Text>
          </View>
          <View
            style={{ width: 65, backgroundColor: "lime", alignSelf: "stretch" }}
          >
            <Text>icon</Text>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            backgroundColor: "blue",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Text style={{ color: "white", fontSize: 25 }}>ScrollView</Text>
        </View>
        <View
          style={{
            alignSelf: "stretch",
            height: 65,
            backgroundColor: "lime",
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            flexDirection:'row'
          }}
        >
          <View style={{ flex: 1,alignSelf: "stretch",borderColor:'white',borderWidth:3,justifyContent:'center',alignItems:'center' }}>
            <Text style={{color:'white',fontSize:30,alignSelf:'center'}}>I</Text>
          </View>
          <View style={{ flex: 1,alignSelf: "stretch",borderColor:'white',borderWidth:3,justifyContent:'center',alignItems:'center' }}>
            <Text style={{color:'white',fontSize:30,alignSelf:'center'}}>C</Text>
          </View>
          <View style={{ flex: 1,alignSelf: "stretch",borderColor:'white',borderWidth:3,justifyContent:'center',alignItems:'center' }}>
            <Text style={{color:'white',fontSize:30,alignSelf:'center'}}>O</Text>
          </View>
          <View style={{ flex: 1,alignSelf: "stretch",borderColor:'white',borderWidth:3,justifyContent:'center',alignItems:'center' }}>
            <Text style={{color:'white',fontSize:30,alignSelf:'center'}}>N</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "auto",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
    display: "flex"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
