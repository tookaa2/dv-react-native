let state = [];
const removeTodo = (state, index) => {
    const temp = state.filter((todo,thisIndex)=>{return index!==thisIndex})
    return temp;
  };
const addTodo = (state, todo) => {
  const temp = state.slice(0);
  const todoitem={
      topic:todo,
      completed:false
  }
  temp.push(todoitem);
  return temp;
};
 deepCopy=(src)=> {
    return JSON.parse(JSON.stringify(src));
  }
const toggleTodo = (state, index) => { 
    let result=[];
    state.map((value,key)=>{
        if(key!==index){
            result.push(value)
        }else{
            const obj=deepCopy(value);
            obj.completed=!obj.completed;
            result.push(obj)
        }
        
    })
  

    return result;
  };

const reducer = (oldState, action) => {
  switch (action.type) {
    case "ADD_TODOS":
      return addTodo(oldState, action.topic);
      break;
      case "TOGGLE_TODOS":
      return toggleTodo(oldState, action.targetIndex);
      break;
      case "REMOVE_TODOS":
      return removeTodo(oldState, action.targetIndex);
      break;
  }
};
state = reducer(state, {
  type: "ADD_TODOS",
  topic: "เอาผ้าไปซัก"
});

console.log(state);
// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     }
// ]

state = reducer(state, {
  type: "ADD_TODOS",
  topic: "รดน้ำต้นไม้"
});

console.log(state);

// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: false
//     }
// ]

state = reducer(state, {
  type: "ADD_TODOS",
  topic: "ซื้อพิซซ่า"
});

console.log(state);

// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: false
//     }
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]

const oldState=state;

state = reducer(state, {
  type: "TOGGLE_TODOS",
  targetIndex: 1
});

console.log('old',oldState);

console.log(state);

// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: true
//     }
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]

state = reducer(state, {
  type: "TOGGLE_TODOS",
  targetIndex: 1
});

console.log(state);

// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'รดน้ำต้นไม้',
//         completed: false
//     }
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]

state = reducer(state, {
  type: "REMOVE_TODOS",
  targetIndex: 1
});

console.log(state);

// Result
// [
//     {
//         topic: 'เอาผ้าไปซัก',
//         completed: false
//     },
//     {
//         topic: 'ซื้อพิซซ่า',
//         completed: false
//     }
// ]
