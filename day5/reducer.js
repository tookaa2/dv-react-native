const productReducer = (state = {}, action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            return {
                id: action.id,
                name: action.name,
                price: action.price,
                description: action.description
            }
        case 'EDIT_PRODUCT':
            return {
                ...state,
                //ป้องกันการไม่ส่งมาบาง attr ไม่ต้องมาใส่ทีละอัน เพราะถ้ามันไม่ส่งมาจะ null แล้วพัง ดูด้านล่าง A1
                ...action.payload
            }
        default:
            return state;
    }
}

// A1--------------------------------
// {
//     type: 'EDIT_PRODUCT',
//         payload: {
//         name: 'smt',
//             price: 5
//     }
// }

//Action creator ------------------------

// const editProductNameAction = (payload) => {
//     return {
//         type: 'EDIT_PRODUCT',
//         payload
//     }
// }
// dispatchEvent(editProductNameAction({
//     name: 'something'
// }))


const productsReducer = (state = {}, action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            return [...state,
            productReducer(null, action)
            ];
        case "EDIT_PRODUCT":
            return state.map((value) => {
                if (value.id === action.product.id) {
                    return productReducer(value, action)
                } else {
                    return value;
                }
            })
        case 'REMOVE_PRODUCT':
            return state.filter((item, index) => index === action.index);
        default:
            return state;
    }
}