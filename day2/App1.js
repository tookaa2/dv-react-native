/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  Modal,
  TouchableHighlight
} from "react-native";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      username: "stuff",
      password: "pass",
      isModalVisible:false,
      modalText:''
    };
  }
  onSubmit=()=>{
    if(this.state.username==="admin"&&this.state.password==="eiei"){
      
      this.setState({isModalVisible:true,modalText:'Correct'})
    }else{
    
      this.setState({isModalVisible:true,modalText:'Incorrect user/pass'})
    }
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "gray", display: "flex" }}>
        <View
          style={{
            flex: 1,
            backgroundColor: "gray",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Image
            source={require("./cat.jpg")}
            style={{ width: 200, height: 200, borderRadius: 200 }}
          />

          <Text>{this.state.username + " - " + this.state.password}</Text>
        </View>
        <View style={{ flex: 1, backgroundColor: "gray" }}>
          <TextInput
            value={this.state.username}
            onChangeText={username => this.setState({ username })}
            style={{
              marginHorizontal: 20,
              backgroundColor: "white",
              height: 60,
              justifyContent: "center",
              fontSize: 30
            }}
          />

          <TextInput
            value={this.state.password}
            style={{
              marginHorizontal: 20,
              backgroundColor: "white",
              height: 60,
              justifyContent: "center",
              fontSize: 30,
              marginTop: 15
            }}
            onChangeText={password => this.setState({ password })}
          />

          <TouchableOpacity
            style={{
              marginHorizontal: 20,
              backgroundColor: "black",
              height: 60,
              position: "absolute",
              bottom: 50,
              left: 0,
              right: 0,
              justifyContent: "center"
            }}
            onPress={()=>{this.onSubmit()}}
          >
            <Text style={{ fontSize: 30, color: "white", alignSelf: "center" }}>
              TouchableOpacity
            </Text>
          </TouchableOpacity>
        </View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.isModalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={{ margin: 50, backgroundColor: 'white', height:100,marginTop:300,borderRadius:10,borderColor:'aqua',borderWidth:2 }}>
            <TouchableHighlight
              onPress={() => {
                this.setState({ isModalVisible: false })
              }}>
              <Text style={{ fontSize: 25 }}>X</Text>
            </TouchableHighlight>
            <View style={{justifyContent:'center'}}>
              <Text style={{ fontSize: 25 }}>{this.state.modalText}</Text>


            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "auto",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
    display: "flex"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
