/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  Modal,
  TouchableHighlight
} from "react-native";

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      currentText: ''
    }
  }
   onPress = (currentText) =>{
    this.setState({ isModalVisible: true,currentText })
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "gray", display: "flex" }}>
        <View
          style={{
            alignSelf: "stretch",
            height: 65,
            backgroundColor: "lime",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "row",
            borderColor: "white",
            borderBottomWidth: 3
          }}
        >
          <View
            style={{ width: 65, backgroundColor: "lime", alignSelf: "stretch" }}
          >
            <Text>icon</Text>
          </View>
          <View
            style={{
              flex: 1,
              alignContent: "center",
              justifyContent: "center",
              borderColor: "white",
              borderLeftWidth: 3,
              borderRightWidth: 3,
              height: 65
            }}
          >
            <Text style={{ alignSelf: "center", color: "white", fontSize: 25 }}>
              Center Text
            </Text>
          </View>
          <View
            style={{ width: 65, backgroundColor: "lime", alignSelf: "stretch" }}
          >
            <Text>icon</Text>
          </View>
        </View>

        <ScrollView
          scrollEnabled={true}
        >
          <View

            style={{

              backgroundColor: "blue",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row"
            }}
          >

            <View style={{flex: 1 }}>
            <TouchableOpacity onPress={()=>{this.onPress('Apple1')}} style={styles.itembox}>
                <Image
                  source={require("./apple.jpg")}
                  style={{ width: 100, height: 100 }}
                />
                <Text style={{ color: "white", fontSize: 25 }}>Apple1</Text>
              </TouchableOpacity >
              <TouchableOpacity onPress={()=>{this.onPress('Apple2')}} style={styles.itembox}>
                <Image
                  source={require("./apple.jpg")}
                  style={{ width: 100, height: 100 }}
                />
                <Text style={{ color: "white", fontSize: 25 }}>Apple2</Text>
              </TouchableOpacity >
              <TouchableOpacity onPress={()=>{this.onPress('Apple3')}} style={styles.itembox}>
                <Image
                  source={require("./apple.jpg")}
                  style={{ width: 100, height: 100 }}
                />
                <Text style={{ color: "white", fontSize: 25 }}>Apple3</Text>
              </TouchableOpacity >
              <TouchableOpacity onPress={()=>{this.onPress('Apple4')}} style={styles.itembox}>
                <Image
                  source={require("./apple.jpg")}
                  style={{ width: 100, height: 100 }}
                />
                <Text style={{ color: "white", fontSize: 25 }}>Apple4</Text>
              </TouchableOpacity >
              <TouchableOpacity onPress={()=>{this.onPress('Apple5')}} style={styles.itembox}>
                <Image
                  source={require("./apple.jpg")}
                  style={{ width: 100, height: 100 }}
                />
                <Text style={{ color: "white", fontSize: 25 }}>Apple5</Text>
              </TouchableOpacity >


            </View>
            <View style={{flex: 1 }}>
            <TouchableOpacity onPress={()=>{this.onPress('Apple6')}} style={styles.itembox}>
                <Image
                  source={require("./apple.jpg")}
                  style={{ width: 100, height: 100 }}
                />
                <Text style={{ color: "white", fontSize: 25 }}>Apple6</Text>
              </TouchableOpacity >
              <TouchableOpacity onPress={()=>{this.onPress('Apple7')}} style={styles.itembox}>
                <Image
                  source={require("./apple.jpg")}
                  style={{ width: 100, height: 100 }}
                />
                <Text style={{ color: "white", fontSize: 25 }}>Apple7</Text>
              </TouchableOpacity >
              <TouchableOpacity onPress={()=>{this.onPress('Apple8')}} style={styles.itembox}>
                <Image
                  source={require("./apple.jpg")}
                  style={{ width: 100, height: 100 }}
                />
                <Text style={{ color: "white", fontSize: 25 }}>Apple8</Text>
              </TouchableOpacity >
              <TouchableOpacity onPress={()=>{this.onPress('Apple9')}} style={styles.itembox}>
                <Image
                  source={require("./apple.jpg")}
                  style={{ width: 100, height: 100 }}
                />
                <Text style={{ color: "white", fontSize: 25 }}>Apple9</Text>
              </TouchableOpacity >
              <TouchableOpacity onPress={()=>{this.onPress('Apple10')}} style={styles.itembox}>
                <Image
                  source={require("./apple.jpg")}
                  style={{ width: 100, height: 100 }}
                />
                <Text style={{ color: "white", fontSize: 25 }}>Apple10</Text>
              </TouchableOpacity >
            </View>

          </View>

        </ScrollView>
        <View
          style={{
            alignSelf: "stretch",
            height: 65,
            backgroundColor: "lime",
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            flexDirection: "row"
          }}
        >
          <View
            style={{
              flex: 1,
              alignSelf: "stretch",
              borderColor: "white",
              borderWidth: 3,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={{ color: "white", fontSize: 30, alignSelf: "center" }}>
              I
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignSelf: "stretch",
              borderColor: "white",
              borderWidth: 3,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={{ color: "white", fontSize: 30, alignSelf: "center" }}>
              C
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignSelf: "stretch",
              borderColor: "white",
              borderWidth: 3,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={{ color: "white", fontSize: 30, alignSelf: "center" }}>
              O
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignSelf: "stretch",
              borderColor: "white",
              borderWidth: 3,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={{ color: "white", fontSize: 30, alignSelf: "center" }}>
              N
            </Text>
          </View>
        </View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.isModalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={{ margin: 50, backgroundColor: 'white', flex: 1 }}>
            <TouchableHighlight
              onPress={() => {
                this.setState({ isModalVisible: false })
              }}>
              <Text style={{ fontSize: 25 }}>X</Text>
            </TouchableHighlight>
            <View style={{justifyContent:'center'}}>
              <Text style={{ fontSize: 25 }}>{this.state.currentText}</Text>


            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "auto",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
    display: "flex"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  itembox:{
    backgroundColor:'pink',
    justifyContent:'center',
    alignContent:'center',
    margin:10
  }
});
