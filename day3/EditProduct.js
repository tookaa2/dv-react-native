import React, { Component } from "react";
import { StyleSheet, Text, View, ScrollView ,TouchableOpacity} from "react-native";
import { commonStyles } from "./Component1";
import { Button, InputItem, List, Icon } from "@ant-design/react-native";
type Props = {};
export default class EditProduct extends Component<Props> {
  constructor(props) {
    super(props);
    console.log(this.props);
  }
  goBack = () => {
    this.props.history.goBack();
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={[commonStyles.menuBar, commonStyles.lightBlue]}>
          <TouchableOpacity
            onPress={this.goBack}
            style={[commonStyles.menuSquare, commonStyles.canaryYellow]}
          >
            <Icon style={[commonStyles.buttonText]} name="left" />
          </TouchableOpacity>
          <Text style={commonStyles.buttonText}>Edit PRODUCT</Text>
        </View>
        <ScrollView scrollEnabled={true} style={{ flex: 1 }}>
          <Button
            style={[commonStyles.button, commonStyles.profileButttonMargin]}
            onPress={() => {}}
            type="primary"
          >
            <Text style={commonStyles.buttonText}>Image</Text>
          </Button>
          <Button
            style={[commonStyles.button, commonStyles.profileButttonMargin]}
            onPress={() => {}}
            type="primary"
          >
            <Text style={commonStyles.buttonText}>Name</Text>
          </Button>
        </ScrollView>
        <View style={[commonStyles.footerButton]}>
          <Button
            style={[commonStyles.button]}
            onPress={() => {}}
            type="primary"
          >
            <Text style={commonStyles.buttonText}>Save</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    // alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
