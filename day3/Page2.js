import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { commonStyles } from "./Component1";

type Props = {};
export default class Page2 extends Component<Props> {
  constructor(props) {
    super(props);
    console.log(this.props);
  }
  goBack = () => {
    this.props.history.goBack();
  };
  onAddProduct= () => {
    this.props.history.push("/AddProduct", { myusername: "123" });
  };
  goProduct = () => {
    this.props.history.push("/Product", { myusername: "123" });
  };
  onProfile = () => {
    this.props.history.push("/Profile", { myusername: "123" });
  };
  renderItems=(items)=>{
   
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={[commonStyles.menuBar, commonStyles.lightBlue]}>
          <Text>{this.props.location.state.myusername}</Text>
        </View>
        <ScrollView scrollEnabled={true} style={{ flex: 1 }}>
          <View
            style={{
              // backgroundColor: "blue",
              flexDirection: "row",
              display: "flex",
              height: 150
            }}
          >
            <TouchableOpacity onPress={this.goProduct} style={commonStyles.gridObject}>
              <Text>Picture1</Text>
            </TouchableOpacity>
            <View style={commonStyles.gridObject}>
              <Text>Picture2</Text>
            </View>
          </View>
          <View
            style={{
              // backgroundColor: "blue",
              flexDirection: "row",
              display: "flex",
              height: 150
            }}
          >
            <View style={commonStyles.gridObject}>
              <Text>Picture3</Text>
            </View>
            <View style={commonStyles.gridObject}>
              <Text>Picture4</Text>
            </View>
          </View>
          <View
            style={{
              // backgroundColor: "blue",
              flexDirection: "row",
              display: "flex",
              height: 150
            }}
          >
            <View style={commonStyles.gridObject}>
              <Text>Picture5</Text>
            </View>
            <View style={commonStyles.gridObject}>
              <Text>Picture6</Text>
            </View>
          </View>
          <View
            style={{
              // backgroundColor: "blue",
              flexDirection: "row",
              display: "flex",
              height: 150
            }}
          >
            <View style={commonStyles.gridObject}>
              <Text>Picture7</Text>
            </View>
            <View style={commonStyles.gridObject}>
              <Text>Picture8</Text>
            </View>
          </View>
          <View
            style={{
              // backgroundColor: "blue",
              flexDirection: "row",
              display: "flex",
              height: 150
            }}
          >
            <View style={commonStyles.gridObject}>
              <Text>Picture9</Text>
            </View>
            <View style={commonStyles.gridObject}>
              <Text>Picture10</Text>
            </View>
          </View>
        </ScrollView>
        <View style={[commonStyles.menuBar, commonStyles.lightBlue]}>
          <TouchableOpacity onPress={this.goBack} style={[commonStyles.menuSquare, commonStyles.canaryYellow]}>
            <Text style={commonStyles.buttonText}>L</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.onAddProduct} style={{ flex: 1 }}>
            <Text style={commonStyles.buttonText}>Add</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={this.onProfile}
            style={[commonStyles.menuSquare, commonStyles.canaryYellow]}
          >
            <Text style={commonStyles.buttonText}>P</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    // alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
