import React, {Component} from 'react'
import { View,Text} from 'react-native'
import {Route,NativeRouter,Switch,Redirect} from 'react-router-native'
import Page1 from './Page1';
import Page2 from './Page2';
import Profile from './Profile';
import EditProfile from './EditProfile';
import Product from './Product';
import EditProduct from './EditProduct';
import AddProduct from './AddProduct';


export default class Router extends Component {
    render(){
        return(
            <NativeRouter>
                <Switch>
                    <Route exact path="/Page1" component={Page1}/>
                    <Route exact path="/Page2" component={Page2}/>
                    <Route exact path="/Profile" component={Profile}/>
                    <Route exact path="/EditProfile" component={EditProfile}/>
                    <Route exact path="/Product" component={Product}/>
                    <Route exact path="/EditProduct" component={EditProduct}/>
                    <Route exact path="/AddProduct" component={AddProduct}/>
                    <Redirect to="/Page1"></Redirect>
                </Switch>
            </NativeRouter>
        )
    }
}