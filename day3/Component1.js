import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";

import { Button, InputItem, List } from "@ant-design/react-native";
import styled from "styled-components/native";
class Component1 extends Component {
  render() {
    return (
      <View>
        <MyButton />
      </View>
    );
  }
}

export const MyButton = styled.TouchableOpacity`
  padding-top: 20;
  background-color: blue;
  border-width: 1;
  border-style: solid;
  border-radius: 5;
`;

export const StandardButton = styled.Button`
  padding-top: 20;
  background-color: blue;
  border-width: 1;
  border-style: solid;
  border-radius: 5;
`;

export const commonStyles = StyleSheet.create({
  button: {
    marginHorizontal: 20,
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  buttonText: {
    fontSize: 30
  },
  loginPosition: {
    position: "absolute",
    bottom: 80,
    left: 0,
    right: 0
  },
  menuBar: {
    height: 60,
    alignSelf: "stretch",
    display: "flex",
    flexDirection: "row"
  },
  menuSquare: {
    width: 60,
    backgroundColor: "white",
    alignSelf: "stretch",
    alignItems: "center",
    justifyContent: "center"
  },
  lightBlue: {
    backgroundColor: "#5BC8AC"
  },
  mist: {
    backgroundColor: "#763626"
  },
  canaryYellow: {
    backgroundColor: "#E6D72A"
  },
  gridObject: {
    flex: 1,
    height: 150,
    margin: 5,
    justifyContent: "center",
    alignItems: "center"
  },
  profileItems: {
    flexDirection: "row",
    height: 70
  },
  footerButton: {
    height: 80,
    alignSelf: "stretch",
    display: "flex",
    flexDirection: "row"
  },
  profileButttonMargin: {
    marginTop: 20
  }
});
export default Component1;
