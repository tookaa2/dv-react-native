import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { connect } from "react-redux";

class Page1 extends Component {
  render() {
    const { todos, completes } = this.props;
    return (
      <View>
        <TouchableOpacity>
          <Text>Add Number</Text>
        </TouchableOpacity>
        <Text>Latest todo is: {todos}</Text>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    todos: state.todos,
    complete: state.complete
  };
};
const mapDispatchToProps = dispatch => {
  return {
    addTodo: topic => {
      dispatch({
        type: "ADD_TODO",
        topic
      });
    }
  };
};
export default connect(
  ({ todos, complete }) => ({ todos, complete }),
  mapDispatchToProps
)(Page1);
