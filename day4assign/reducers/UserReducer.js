export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "SET_USERNAME":
      return { ...state, username: action.name }
      case "SET_FIRSTNAME":
      return { ...state, firstname: action.name }
      case "SET_LASTNAME":
      return { ...state, lastname: action.name }
    case "TOGGLE_COMPLETE":
      return state.map((item, index) => {
        return index === action.index ? !item : item;
      });
    case "REMOVE_COMPLETE":
      return state.filter((item, index) => index !== action.index);
    default:
      return state;
  }
};

const INITIAL_STATE = {
  username: '',
  firstname:'',
  lastname:''
}

