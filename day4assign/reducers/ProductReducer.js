export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "ADD_PRODUCT":
      return {
        products: [...state.products, action.product]
      }
    case "EDIT_PRODUCT":
      return {
        products: state.products.map((value, key) => {
          if (value.id === action.product.id) {
            return {
              image: action.product.image,
              name: action.product.name,
              id: value.id
            }
          } else {
            return value;
          }

        })
      }
    default:
      return state;
  }
};

const INITIAL_STATE = {
  products: [
    { image: require('../cat.jpg'), name: 'Product1', id: '6par7p435' },
    { image: require('../cat.jpg'), name: 'Product2', id: 'w9nirf1n6' },
    { image: require('../cat.jpg'), name: 'Product3', id: '6k3rdoijy' },
    { image: require('../cat.jpg'), name: 'Product4', id: '6k3rdoijx' },
    { image: require('../cat.jpg'), name: 'Product5', id: '6k3rdoija' },
    { image: require('../cat.jpg'), name: 'Product6', id: '6k3rdoeey' },
    { image: require('../cat.jpg'), name: 'Product7', id: '6k3rdoqUy' },
    { image: require('../cat.jpg'), name: 'Product8', id: '6k3rdoi48' },
    { image: require('../cat.jpg'), name: 'Product9', id: '6k3rdoire' },
    { image: require('../cat.jpg'), name: 'Product10', id: '6k3rdoigE' }]
}

const fakeData = [
  { image: require('../cat.jpg'), name: 'Product1' },
  { image: require('../cat.jpg'), name: 'Product2' },
  { image: require('../cat.jpg'), name: 'Product3' },
  { image: require('../cat.jpg'), name: 'Product4' },
  { image: require('../cat.jpg'), name: 'Product5' },
  { image: require('../cat.jpg'), name: 'Product6' },
  { image: require('../cat.jpg'), name: 'Product7' },
  { image: require('../cat.jpg'), name: 'Product8' },
  { image: require('../cat.jpg'), name: 'Product9' },
  { image: require('../cat.jpg'), name: 'Product10' }];