import React, { Component } from "react";
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from "react-native";
import { commonStyles } from "../Component1";
import { Button, InputItem, Icon } from "@ant-design/react-native";
import { connect } from "react-redux";
type Props = {};
class EditProfile extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      firstname: '',
      lastname: ''
    }

  }
  goBack = () => {
    this.props.history.goBack();
  };
  onSubmit = () => {
    this.props.setFName(this.state.firstname)
    this.props.setLName(this.state.lastname)
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={[commonStyles.menuBar, commonStyles.lightBlue]}>
          <TouchableOpacity
            onPress={this.goBack}
            style={[commonStyles.menuSquare, commonStyles.canaryYellow]}
          >
            <Icon style={[commonStyles.buttonText]} name="left" />
          </TouchableOpacity>
          <Text style={commonStyles.buttonText}>Edit Profile</Text>
        </View>
        <ScrollView scrollEnabled={true} style={{ flex: 1 }}>
          <InputItem
            onFocus={() => {
              this.setState({ firstname: "" });
            }}
            value={this.state.firstname}
            onChange={firstname => this.setState({ firstname })}
            extra=""
            placeholder="First name"
          />
          <InputItem
            onFocus={() => {
              this.setState({ lastname: "" });
            }}
            value={this.state.lastname}
            onChange={lastname => this.setState({ lastname })}
            extra=""
            placeholder="Last name"
          />
        </ScrollView>
        <View style={[commonStyles.footerButton]}>
          <Button
            style={[commonStyles.button]}
            onPress={this.onSubmit}
            type="primary"
          >
            <Text style={commonStyles.buttonText}>Save</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    // alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
const mapStateToProps = state => {
  return {
    user: state.user
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setFName: name => {
      dispatch({
        type: "SET_FIRSTNAME",
        name
      });
    },
    setLName: name => {
      dispatch({
        type: "SET_LASTNAME",
        name
      });
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProfile);
