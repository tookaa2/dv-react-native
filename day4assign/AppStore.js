import { createStore, combineReducers ,applyMiddleware} from "redux";
import logger from 'redux-logger'
// import TodosReducer from "./TodosReducer";
import UserReducer from "./reducers/UserReducer";
import ProductReducer from './reducers/ProductReducer'
const reducers = combineReducers({
  user: UserReducer,
  product:ProductReducer
});
const store = createStore(reducers, applyMiddleware(logger));
console.log(store.getState());
export default store;