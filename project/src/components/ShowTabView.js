import * as React from 'react';
import { View, StyleSheet, Dimensions, Text, TouchableOpacity, FlatList } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import ShowTimePane from '../containers/ShowTimePaneContainer'
import SwiperFlatList from 'react-native-swiper-flatlist';
const { width } = Dimensions.get('window')

export default class ShowTabView extends React.Component {
    constructor(props) {
        super(props)
        console.log('SHOW TIME TAB', this.props.movieId)
    }

    state = {
        index: 0,
        routes: [
            { key: '1', title: 'First', _id: this.props.movieId },
            { key: '2', title: 'Second', _id: this.props.movieId },
            { key: '3', title: 'Third', _id: this.props.movieId },
            { key: '4', title: 'Fourth', _id: this.props.movieId },
            { key: '5', title: 'Fifth', _id: this.props.movieId },
            { key: '6', title: 'Six', _id: this.props.movieId },
            { key: '7', title: 'Seven', _id: this.props.movieId },
        ],
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 20 }}>
                    <FlatList
                        horizontal={true}
                        style={{ height: 20 }}
                        data={[{ key: '1', name: 'Today' }, { key: '2', name: 'Date' }, { key: '3', name: 'Date' }, { key: '4', name: 'Date' }, { key: '5', name: 'Date' }, { key: '6', name: 'Date' }, { key: '7', name: 'Date' }]}
                        renderItem={({ item }) => <TouchableOpacity style={{ width: width / 7, height: 20 }} onPress={() => { this.swiperRef.scrollToIndex(item.key - 1) }}><Text>{item.name}</Text></TouchableOpacity>}
                    />
                </View>



                <SwiperFlatList ref={swiper => { this.swiperRef = swiper }} key="DateSwiper" >
                    <ShowTimePane key="showPaneDayIndex1" dayIndex={1} _id={this.props.movieId} />
                    <ShowTimePane key="showPaneDayIndex2" dayIndex={2} _id={this.props.movieId} />
                    <ShowTimePane key="showPaneDayIndex3" dayIndex={3} _id={this.props.movieId} />
                    <ShowTimePane key="showPaneDayIndex4" dayIndex={4} _id={this.props.movieId} />
                    <ShowTimePane key="showPaneDayIndex5" dayIndex={5} _id={this.props.movieId} />
                    <ShowTimePane key="showPaneDayIndex6" dayIndex={6} _id={this.props.movieId} />
                    <ShowTimePane key="showPaneDayIndex7" dayIndex={7} _id={this.props.movieId} />
                </SwiperFlatList>
                {
                    // <Text>TOP MOVIE BAR</Text>
                    // <TabView
                    //     navigationState={this.state}
                    //     renderScene={SceneMap({
                    //         1: ShowTimePane,
                    //         2: ShowTimePane,
                    //         3: ShowTimePane,
                    //         4: ShowTimePane,
                    //         5: ShowTimePane,
                    //         6: ShowTimePane,
                    //         7: ShowTimePane
                    //     })}
                    //     onIndexChange={index => this.setState({ index })}
                    //     initialLayout={{ width: Dimensions.get('window').width }}
                    // />
                }

            </View>

        );
    }
}

const styles = StyleSheet.create({
    scene: {
        flex: 1,
    },
});