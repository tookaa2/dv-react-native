import React from 'react'
import { View, Text, SectionList, TouchableOpacity, Dimensions } from 'react-native'
import { getSingleShowtime } from '../utils/dataHelper'
import _ from 'lodash'
import { ROUTE_SEATSELECT } from '../constants/RouteConstants'
import dateFormat from 'dateformat'
import { displayMessage } from '../utils/toasterHelper'
const { width } = Dimensions.get('window')
class ShowTimePane extends React.Component {
    constructor(props) {
        super(props)
        console.log('Pane prop', this.props)
        this.state = {
            showtime: []
        }
    }
    parseByCinema = (dataObj) => {
        let head = [];
        let output = [];

        _.forEach(dataObj, (value, key) => {
            if (!_.includes(head, value.soundtrack)) {
                head.push(value.soundtrack)
            }
        })
        _.forEach(head, (headVal) => {
            let toPush = [];
            _.forEach(dataObj, (value) => {
                if (_.isEqual(value.soundtrack, headVal)) {
                    toPush.push(value);
                }
            })
            output.push({
                title: headVal,
                data: _.cloneDeep(toPush)
            })
        })
        return output;

    }
    loadDetails = async () => {
        console.log('LOAD DETAIl CALLED');

        const today = new Date()
        today.setDate(today.getDate() + (this.props.dayIndex - 1));
        const response = await getSingleShowtime(this.props._id, today);
        console.log('loading details for pane')
        console.log(response.data);
        const sorted = _.sortBy(response.data, movie => {
            return movie.startDateTime
        })
        console.log(sorted)

        this.setState({ showtime: this.parseByCinema(sorted) })
    }
  
    checkTime = (inputDate) => {
        const now = new Date()
        let timeDifference = inputDate.getTime() - now.getTime()
        return timeDifference < 3600000
    }
    onPressTime = (item, locked) => {
        if (!locked) {
            this.props.push(ROUTE_SEATSELECT, { details: item })
            this.props.clearSeat()
        }
        else {
            displayMessage('Cannot book this showtime, over time limit')
        }
    }

    _renderTimes = (inputObj) => {

        console.log('recieve for render', inputObj)
        return inputObj.map(item => {
            const date = new Date(item.startDateTime);

            return (
                <TouchableOpacity
                    style={{ justifyContent: 'center', width: 100, backgroundColor: this.checkTime(date) ? 'grey' : 'white', height: 50, borderRadius: 5, margin: 5, borderWidth: 1, borderColor: 'gray' }}
                    onPress={() => { this.onPressTime(item, this.checkTime(date)) }}>

                    <Text style={{ fontSize: 23, alignSelf: 'center', color: this.checkTime(date) ? 'white' : 'black' }}>{dateFormat(date, "hh:MM")}</Text>




                </TouchableOpacity >
            )
        })

    }
    _renderHeadWithContent = (inputObj) => {
        return (

            this.state.showtime.map(showtime => {
                return (
                    <View>
                        <View>
                            <Text>{_.upperCase(showtime.title)}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', width: width }}>

                            {
                                this._renderTimes(showtime.data)
                            }
                        </View>
                    </View>
                )
            })
        )
    }
    componentWillMount() {
        this.loadDetails()
    }
    componentDidMount() {

        console.log('state', this.state)
    }
    render() {
        return (
            <View style={{ flex: 1, borderColor: 'black', borderWidth: 1, width: width }} >
                <View style={{ backgroundColor: 'orange' }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 20 }}>CHIANG MAI CINEMA</Text>
                </View>

                {
                    this.state.showtime[0] ?
                        <View style={{ flex: 1 }}>

                            {
                                this._renderHeadWithContent(this.state.showtime)

                            }




                        </View>

                        : <Text>NO INFORMATION</Text>
                }


            </View>
        )
    }
}


export default ShowTimePane