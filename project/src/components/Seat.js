import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image';
import { displayMessage } from '../utils/toasterHelper'
const tickIcon = require('../assets/tick.png')
const lockIcon = require('../assets/lock.png')
const lockSofaIcon = require('../assets/locksofa.png')
class SeatRow extends React.Component {
    constructor(props) {
        super(props)
    }
    state = {
        isSelected: false,
        isLocked: this.props.isLocked
    }
    componentDidMount() {
       

    }
    onSelect = () => {
        const { seatType, row, column } = this.props
        console.log('Row', row)
        console.log('Type', seatType)
        console.log('column', column)
        if (!this.state.isLocked) {
            if (!this.state.isSelected) {
                this.props.addSeatSelection({
                    type: seatType,
                    row,
                    column
                })
                if (seatType === "SOFA") {
                    this.props.addSeatSelection({
                        type: seatType,
                        row,
                        column: column + 1
                    })
                }
            } else {
                this.props.removeSeatSelection({
                    type: seatType,
                    row,
                    column
                })
                if (seatType === "SOFA") {
                    this.props.removeSeatSelection({
                        type: seatType,
                        row,
                        column: column + 1
                    })
                }

            }
            this.setState({
                isSelected: !this.state.isSelected
            })
        } else {

            displayMessage('Seat already sold')
        }

    }
    render() {
        const { seatType, row, column } = this.props
        return (
            <TouchableOpacity onPress={this.onSelect}>
                {this.state.isLocked ?
                    seatType === "SOFA" ?
                        <AutoHeightImage width={16} source={lockSofaIcon} style={{ marginHorizontal: 4 }} />
                        :
                        <AutoHeightImage width={8} source={lockIcon} style={{ margin: 3 }} />
                    :
                    <AutoHeightImage width={seatType === "SOFA" ? 16 : 8} source={this.props.seatIcon} style={{ margin: seatType === "SOFA" ? 5 : 3, opacity: this.state.isSelected ? 0.45 : 1 }} />
                }

                {this.state.isSelected ?
                    <View
                        style={seatType === "SOFA" ? { margin: 3, position: 'absolute', left: 0, top: 0 } : { position: 'absolute', alignSelf: 'center' }}
                    >
                        <AutoHeightImage width={10} source={tickIcon} />
                    </View>

                    : null}
                {this.state.isSelected && seatType === "SOFA" ? <AutoHeightImage width={10} source={tickIcon} style={{ margin: 3, position: 'absolute', top: 0, right: 0 }} /> : null}
            </TouchableOpacity>
        )
    }
}


export default SeatRow