import React from 'react'
import { View, Text } from 'react-native'
import Seat from '../containers/SeatContainer'
class SeatRow extends React.Component {
    constructor(props) {
        super(props)

    }
    state = {
    }
    componentDidMount() {

    }
    render() {
        return (
            <View style={{ flexDirection: 'row', width: 260, justifyContent: 'center' }}>
                {
                    this.props.rowData.map((attribute, index) => {
                        if (this.props.seatType === "SOFA") {
                            if (index % 2 === 0) {
                                return <Seat isLocked={attribute} seatIcon={this.props.seatIcon} seatType={this.props.seatType} row={this.props.rowNumber} column={index + 1} />
                            }
                        }
                        else {
                            return <Seat isLocked={attribute} seatIcon={this.props.seatIcon} seatType={this.props.seatType} row={this.props.rowNumber} column={index + 1} />
                        }




                    })
                }

            </View>
        )
    }
}


export default SeatRow