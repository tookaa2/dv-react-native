import React from 'react'
import { View, Text, StyleSheet,TouchableWithoutFeedback } from 'react-native'
import {

    ROUTE_PROFILE,
    ROUTE_HISTORY,
    ROUTE_MOVIES,
    ROUTE_PROMOTION

} from '../constants/RouteConstants'
import { Icon } from '@ant-design/react-native'
import { Link } from 'react-router-native'

class Footer extends React.Component {
    constructor(props) {
        super(props)
        console.log('FOOTER', this.props)
        this.state={
            currentRoute:"MOVIE"
        }
    }
    state = {
    }
    componentDidMount() {

    }
    render() {
        return (
            <View style={{
                backgroundColor: 'pink',
                flexDirection: 'row',
                position: 'absolute',
                height: '8%',
                bottom: 0,
                right: 0,
                left: 0,
                opacity: this.props.user.isloggedIn ? 1 : 0
            }}>
                <Link onPress={()=>{this.setState({currentRoute:"MOVIE"})}} to={ROUTE_MOVIES} style={this.state.currentRoute==="MOVIE"?styles.tabContainer:styles.tabInactive}>
                    <Text>
                        <Icon name='check' /> To login
                    </Text>
                </Link>
                <Link onPress={()=>{this.setState({currentRoute:"HISTORY"})}} to={ROUTE_PROMOTION} style={this.state.currentRoute==="HISTORY"?styles.tabContainer:styles.tabInactive}>

                    <Text>
                        <Icon name='tag' />
                        To PROMO
                    </Text>
                </Link>
                <Link onPress={()=>{this.setState({currentRoute:"USER"})}} to={ROUTE_PROFILE} style={this.state.currentRoute==="USER"?styles.tabContainer:styles.tabInactive}>
                    <Text>
                        <Icon name='user' />
                        To Profile
                    </Text>
                </Link>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    tabContainer: {
        flex: 1,
        backgroundColor:'white',
        // borderRadius:80,
        // margin:10
    },
    tabInactive: {
        flex: 1,
        marginTop:10
    },
});
export default Footer