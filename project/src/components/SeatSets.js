import React from 'react'
import { View, Text } from 'react-native'
import SeatRow from './SeatRow'
const premiumLabel = ['B', 'C', 'D', 'E']
const deluxeLabel = ['F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N']
class SeatSets extends React.Component {
    constructor(props) {
        super(props)
    }
    state = {
    }
    componentDidMount() {

    }

    _getSeatLabel = (rowName) => {

        if (this.props.seatType === "SOFA") {
            return "A"
        } else if (this.props.seatType === "PREMIUM") {
            return premiumLabel[rowName.replace('row', '')]
        } else {
            return deluxeLabel[rowName.replace('row', '')]
        }
    }
    render() {
        return (
            <View style={{ flexDirection: 'column', marginTop: 18 }}>
                {
                    Object.keys(this.props.seatData).map((rowName, index) => {
                        return <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ width: 30, justifyContent: 'center', marginRight: 12 }}><Text style={{ alignSelf: 'center' }}>{this._getSeatLabel(rowName)}</Text></View>
                            <SeatRow rowData={this.props.seatData[rowName]} seatType={this.props.seatType} seatIcon={this.props.seatIcon} rowNumber={rowName.replace('row', '')} />
                            <View style={{ width: 30, justifyContent: 'center', marginLeft: 12 }}><Text style={{ alignSelf: 'center' }}>{this._getSeatLabel(rowName)}</Text></View>
                        </View>

                    })
                }

            </View>
        )
    }
}


export default SeatSets