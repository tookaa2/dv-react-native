import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image';
import { getShowbyID } from '../utils/dataHelper'
import dateFormat from 'dateformat'
const soundIcon = require('../assets/soundtrack.png')
const subtitleIcon = require('../assets/subtitle.png')
const dots = require('../assets/dots.png')
const premiumLabel = ['B', 'C', 'D', 'E']
const deluxeLabel = ['F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N']
class HistoryContent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            image: '',
            movieName: '',
            startTime: '',
            endTime: '',
            soundTrack: '',
            subtitle: '',
            duration: 0,
            cinema: 0
        }
        this.getMovieDetails()
    }
    _forSeats = () => {
        let seats = ''
        this.props.item.seats.map(seat => {
            console.log(seat)
            seats += this._getSeatLabel(seat.row - 1, seat.type, seat.column) + ' '
        })
        return seats
    }
    _getSeatLabel = (row, seatType, column) => {
        // console.log('they got row', row)
        // console.log('they got seatType', seatType)
        // console.log('they got column', column)
        if (seatType === "SOFA") {
            return "A" + column
        } else if (seatType === "PREMIUM") {
            return premiumLabel[row] + column
        } else {
            return deluxeLabel[row] + column
        }

    }
    getMovieDetails = async () => {
        const result = await getShowbyID(this.props.item.showtime)
        console.log('History get', result)
        this.setState({
            image: result.movie.image,
            movieName: result.movie.name,
            startTime: result.startDateTime,
            endTime: result.endDateTime,
            soundTrack: result.soundtrack,
            subtitle: result.subtitle,
            duration: result.movie.duration,
            cinema: result.cinema.name
        })
    }
    componentDidMount() {

    }
    onSelect = () => {

    }
    calTotalPrice = (seats) => {
        let sum = 0;
        seats.map(obj => {
            if (obj.type === "PREMIUM") {
                sum += 190
            } else if (obj.type === "SOFA") {
                sum += 500
            } else {
                sum += 150
            }
        })

        return sum
    }
    onHistory = () => {
        this.props.history.push('/confirm', {
            seats: this._forSeats(),
            cinema: this.state.cinema,
            time: dateFormat(this.state.startTime, "hh:MM"),
            price: this.calTotalPrice(this.props.item.seats),
            date: dateFormat(this.state.startTime, "dd mmm yyyy"),
            movieImage: this.state.image,
            movieName: this.state.movieName,
            qr: this.props.item.qrCode
        })

    }
    render() {
        const { item, style } = this.props
        return (
            <TouchableOpacity style={style} onPress={this.onHistory}>

                <AutoHeightImage
                    width={126}
                    source={{ uri: `${this.state.image}` }}
                    style={{}}
                />
                <View style={{ position: 'absolute', left: 120 }}>
                    <AutoHeightImage
                        width={12}
                        source={dots}

                    />
                </View>
                <View style={{ flexDirection: 'column', paddingLeft: 20 }}>

                    <View>
                        <Text style={{ color: 'blue', fontWeight: 'bold', fontSize: 20 }}>{this.state.movieName}</Text>
                    </View>

                    <View style={{ flexDirection: 'row' }}>

                        <Text>{this.state.cinema} | </Text>
                        <AutoHeightImage
                            width={18}
                            source={soundIcon}
                            style={{ alignSelf: 'center' }}
                        />

                        <Text> {this.state.soundTrack}  </Text>
                        <AutoHeightImage
                            width={18}
                            source={subtitleIcon}
                            style={{ alignSelf: 'center' }}
                        />
                        <Text> {this.state.subtitle}</Text>
                    </View>
                    <View style={{ borderColor: 'black', borderWidth: 1, borderRadius: 3, width: 100 }}>
                        <Text>{dateFormat(this.state.startTime, "hh:MM")} - {dateFormat(this.state.endTime, "hh:MM")}</Text>
                    </View>


                    <Text style={{ fontWeight: 'bold', fontSize: 20 }}>Date: {dateFormat(this.state.startTime, "dd mmm yyyy")}</Text>
                </View>

            </TouchableOpacity>
        )
    }
}


export default HistoryContent