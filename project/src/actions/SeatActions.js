import _ from 'lodash'
export const setSeats = (payload) => ({
    type: 'SET_SEAT',
    payload
})
export const addSeatSelection = (input) => ({
    type: 'ADD_SELECTION',
    payload: input
})
export const removeSeatSelection = (input) => ({
    type: 'REMOVE_SELECTION',
    payload: input
})

export const clearSeat = () => ({
    type: 'CLEAR_SELECTION'
})


export const assembleSeats = (seatData) => {
    const reverseSeat = seatData.slice(0).reverse().map(value => value)
    const DELUXE = {};
    const SOFA = {};
    const PREMIUM = {};
    reverseSeat.map((seatType, label) => {
        Object.keys(seatType).map((attribute, index) => {
            if (_.startsWith(attribute, 'row') && !_.isEqual('rows', attribute)) {
                if (seatType.type === "DELUXE") {
                    DELUXE[attribute] = seatType[attribute]
                }
                else if (seatType.type === "SOFA") {
                    SOFA[attribute] = seatType[attribute]
                } else if (seatType.type === "PREMIUM") {
                    PREMIUM[attribute] = seatType[attribute]
                }
            }
        });

    });
    return {
        DELUXE,
        SOFA,
        PREMIUM
    }
}