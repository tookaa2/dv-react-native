export const setUser = (payload) => ({
    type: 'USER_SAVE',
    payload
})
export const clearUser = (payload) => ({
    type: 'USER_CLEAR',
    payload
})
export const setImage = (payload) => ({
    type: 'USER_IMAGECHANGE',
    payload
})
