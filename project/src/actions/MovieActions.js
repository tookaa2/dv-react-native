export const movieAdd = ( list ) => ({
    type: 'MOVIE_SET_LIST',
    payload: { list }
})
