import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import UserReducer from '../reducers/UserReducer'
import MovieReducer from '../reducers/MovieReducer'
import SeatReducer from '../reducers/SeatReducer';
const reducers = history =>
    combineReducers({
        user: UserReducer,
        movie:MovieReducer,
        router: connectRouter(history),
        seat:SeatReducer
    })

export default reducers