import { createMemoryHistory } from 'history'
import { applyMiddleware, createStore, compose } from 'redux'
import { routerMiddleware } from 'connected-react-router'
import logger from 'redux-logger'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native

import createReducer from './Reducers'

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['user']
}

export const history = createMemoryHistory()
export const store = createStore(
    persistReducer(persistConfig, createReducer(history)),

    compose(applyMiddleware(routerMiddleware(history), logger))
)
export const persistor = persistStore(store)