import React from 'react'
import styled from 'styled-components'
import { Route, Switch, Redirect, Link } from 'react-router-native'
import { BottomNavigation, Tab } from 'react-router-native'
import { View, Text, StyleSheet } from 'react-native'
import { Grid, Icon } from '@ant-design/react-native'
import {
    ROUTE_LOGIN,
    ROUTE_SIGNUP,
    ROUTE_PROFILE,
    ROUTE_HISTORY,
    ROUTE_MOVIES,
    ROUTE_MOVIEDETAIL,
    ROUTE_SHOWTIME,
    ROUTE_SEATSELECT,
    ROUTE_CONFIRM,
    ROUTE_EDIT,
    ROUTE_PROMOTION
} from '../constants/RouteConstants'
import { SignupPage } from '../pages'
import Footer from '../containers/FooterContainer'
import ProfilePage from '../containers/ProfileContainer'
import HistoryPage from '../containers/HistoryContainer'
import MovieList from '../containers/MovielistContainer'
import LoginPage from '../containers/LoginContainer'
import MovieDetailsPage from '../containers/MovieDetailsContainer'
import ShowtimePage from '../pages/ShowtimePage'
import SeatSelectPage from '../containers/SeatSelectContainer'
import ConfirmPage from '../pages/ConfirmPage'
import EditInfoPage from '../pages/EditInfoPage'
import PromotionPage from '../pages/Promotion.js'
export default class Router extends React.Component {
    constructor(props) {
        super(props)
        console.log('ROUTER', this)
    }
    render() {
        return (
            <DefaultBackground>

                <Route exact path={ROUTE_LOGIN} component={LoginPage} />
                <Route exact path={ROUTE_SIGNUP} component={SignupPage} />
                <Route exact path={ROUTE_PROFILE} component={ProfilePage} />
                <Route exact path={ROUTE_HISTORY} component={HistoryPage} />
                <Route exact path={ROUTE_MOVIES} component={MovieList} />
                <Route exact path={ROUTE_MOVIEDETAIL} component={MovieDetailsPage} />
                <Route exact path={ROUTE_SHOWTIME} component={ShowtimePage} />
                <Route exact path={ROUTE_SEATSELECT} component={SeatSelectPage} />
                <Route exact path={ROUTE_CONFIRM} component={ConfirmPage} />
                <Route exact path={ROUTE_EDIT} component={EditInfoPage} />
                <Route exact path={ROUTE_PROMOTION} component={PromotionPage}/>
                <Footer />
            </DefaultBackground>
        )
    }
}

const DefaultBackground = styled.SafeAreaView`
    background-color: white;
    flex: 1;
    flex-direction: column;`

const styles = StyleSheet.create({
    tabContainer: {
        flex: 1
    }
});