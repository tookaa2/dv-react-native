import React, { Component } from 'react';
import { ConnectedRouter } from 'connected-react-router'
import { Provider } from 'react-redux'
import { store, history,persistor } from './system/Store.js'
import Router from './system/Router.js'
import { PersistGate } from 'redux-persist/integration/react'
type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ConnectedRouter history={history}>
          <Router />
        </ConnectedRouter>
        </PersistGate>
      </Provider>

    );
  }
}