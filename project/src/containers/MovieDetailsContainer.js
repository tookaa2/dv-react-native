import { connect } from 'react-redux'
import { movieAdd } from '../actions/MovieActions'
import MovieDetailsPage from '../pages/MovieDetailsPage'

const mapStateToProps = ({ movie, user }) => ({
    movie,
    user
})
// const mapDispatchToProps = dispatch => ({
//     itemAdd: () => {
//         dispatch(itemAdd)
//     },
// })

export default connect(
    mapStateToProps,
    {
        movieAdd
    }
)(MovieDetailsPage)
