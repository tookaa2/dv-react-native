import { connect } from 'react-redux'
import { movieAdd } from '../actions/MovieActions'
import { setUser } from '../actions/UserActions'
import LoginPage from '../pages/LoginPage'

const mapStateToProps = ({ movie, user }) => ({
    movie,
    user
})

export default connect(
    mapStateToProps,
    {
        movieAdd,
        setUser
    }
)(LoginPage)
