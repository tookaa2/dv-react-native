import { connect } from 'react-redux'
import { movieAdd } from '../actions/MovieActions'
import { clearSeat } from '../actions/SeatActions'
import ShowTimePane from '../components/ShowTimePane'
import { push } from 'connected-react-router'
const mapStateToProps = ({ movie, user }) => ({
    movie,
    user
})
// const mapDispatchToProps = dispatch => ({
//     movieAdd: () => {
//         dispatch(movieAdd)
//     },
// })

export default connect(
    mapStateToProps,
    {
        movieAdd,
        push,
        clearSeat
    }
)(ShowTimePane)
