import { connect } from 'react-redux'
import { movieAdd } from '../actions/MovieActions'
import MovieList from '../pages/MovielistPage'

const mapStateToProps = ({ movie, user }) => ({
    movie,
    user
})
// const mapDispatchToProps = dispatch => ({
//     itemAdd: () => {
//         dispatch(itemAdd)
//     },
// })

export default connect(
    mapStateToProps,
    {
        movieAdd
    }
)(MovieList)
