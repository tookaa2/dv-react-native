import { connect } from 'react-redux'
import Footer from '../components/Footer'

const mapStateToProps = ({ movie, user, seat }) => ({
    movie,
    user,
    seat
})

export default connect(
    mapStateToProps,
    {}
)(Footer)
