import { connect } from 'react-redux'
import { setSeats } from '../actions/SeatActions'
import SeatSelectPage from '../pages/SeatSelectPage'

const mapStateToProps = ({ movie, user, seat }) => ({
    movie,
    user,
    seat
})

export default connect(
    mapStateToProps,
    {
        setSeats
    }
)(SeatSelectPage)
