import { connect } from 'react-redux'
import { movieAdd } from '../actions/MovieActions'
import History from '../pages/History'

const mapStateToProps = ({ movie, user }) => ({
    movie,
    user
})
// const mapDispatchToProps = dispatch => ({
//     itemAdd: () => {
//         dispatch(itemAdd)
//     },
// })

export default connect(
    mapStateToProps,
    {
        movieAdd
    }
)(History)
