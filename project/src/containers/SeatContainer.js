import { connect } from 'react-redux'
import { addSeatSelection, removeSeatSelection } from '../actions/SeatActions'
import Seat from '../components/Seat'

const mapStateToProps = ({ movie, user, seat }) => ({
    movie,
    user,
    seat
})

export default connect(
    mapStateToProps,
    {
        addSeatSelection, removeSeatSelection
    }
)(Seat)
