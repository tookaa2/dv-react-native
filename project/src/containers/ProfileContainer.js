import { connect } from 'react-redux'
import { movieAdd } from '../actions/MovieActions'
import { clearUser, setImage } from '../actions/UserActions'
import ProfilePage from '../pages/ProfilePage'

const mapStateToProps = ({ movie, user }) => ({
    movie,
    user
})

export default connect(
    mapStateToProps,
    {
        movieAdd,
        clearUser,
        setImage
    }
)(ProfilePage)
