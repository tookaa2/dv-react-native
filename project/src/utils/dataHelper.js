import axios from 'axios'
const changeProfile = async (formData, token) => {
    return new Promise((res, rej) => {
        axios.post('https://zenon.onthewifi.com/ticGo/users/image',
            formData
            , {
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            }).then(function (response) {
                console.log('got Image!')
                console.log(response.data)
                res(response.data)
            })
            .catch(function (error) {
                console.log('IMAGE CHANGE ERROR')
                console.log('showId', showtimeId)
                console.log('seats', seats)
                console.log('token', token)
                console.log(error);
                rej(error)

            });
    })
}
const getUserInfos = async (token) => {
    return new Promise((res, rej) => {
        axios.get('https://zenon.onthewifi.com/ticGo/users', {
            headers: {
                Authorization: `Bearer ${token}`,
            }
        }).then(function (response) {
            console.log('got History!')
            console.log(response.data)
            res(response.data)
        })
            .catch(function (error) {
                console.log('History ERROR')
                console.log(error);
                rej(error)

            });
    })
}
const getHistory = async (token) => {
    return new Promise((res, rej) => {
        axios.get('https://zenon.onthewifi.com/ticGo/movies/book', {
            headers: {
                Authorization: `Bearer ${token}`,
            }
        }).then(function (response) {
            console.log('got History!')
            console.log(response.data)
            res(response.data)
        })
            .catch(function (error) {
                console.log('History ERROR')
                console.log(error);
                rej(error)

            });
    })
}

const buyTicker = async (showtimeId, seats, token) => {
    return new Promise((res, rej) => {
        axios.post('https://zenon.onthewifi.com/ticGo/movies/book', {
            showtimeId,
            seats
        }, {
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            }).then(function (response) {
                console.log('got ticket!')
                console.log(response.data)
                res(response.data)
            })
            .catch(function (error) {
                console.log('BOOKING ERROR')
                console.log('showId', showtimeId)
                console.log('seats', seats)
                console.log('token', token)
                console.log(error);
                rej(error)

            });
    })
}
const logIn = async (email, password) => {
    return new Promise((res, rej) => {
        axios.post('https://zenon.onthewifi.com/ticGo/users/login', {
            email,
            password
        }).then(function (response) {
            console.log('got res')
            res(response.data.user)
        })
            .catch(function (error) {
                console.log(error);
                rej(error)

            });
    })

}
const editProfile = async (firstName, lastName, token) => {
    return new Promise((res, rej) => {
        axios.put('https://zenon.onthewifi.com/ticGo/users', {
            firstName,
            lastName
        }, {
                headers: {
                    Authorization: `Bearer ${token}`,
                }
            }).then(function (response) {
                console.log('got info from edit')
                res(response.data.user)
            })
            .catch(function (error) {
                console.log(error);
                rej(error)

            });
    })

}
const register = async (email, firstName, lastName, password) => {
    return new Promise((res, rej) => {
        axios.post('https://zenon.onthewifi.com/ticGo/users/register', {
            email,
            password,
            firstName,
            lastName
        }).then(function (response) {
            console.log('got res')
            res(response.data.user)
        })
            .catch(function (error) {
                console.log(error);
                rej(error)

            });
    })

}
const getMovies = async () => {
    return new Promise((res, rej) => {
        axios.get('https://zenon.onthewifi.com/ticGo/movies').then(
            (response) => {
                console.log(response)
                res(response)
            }
        ).catch(err => rej(err))
    })
}
const getSingleShowtime = async (movieId, date) => {
    return new Promise((res, rej) => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${movieId}`, {
            params:
            {
                date: date.getTime()
            }
        }
        ).then(
            (response) => {
                console.log(response)
                res(response)
            }
        ).catch(err => rej(err))
    })
}
const getShowbyID = async (movieId, date) => {
    return new Promise((res, rej) => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/${movieId}`
        ).then(
            (response) => {
                res(response.data)
            }
        ).catch(err => rej(err))
    })
}
const getDetails = async (title) => {
    return new Promise((res, rej) => {
        axios.get(`http://www.omdbapi.com/?t=${title}&plot=full&apikey=89f0404e`).then(
            (response) => {
                console.log(response)
                res(response)
            }
        ).catch(err => rej(err))
    })
}

export { logIn, getMovies, register, getDetails, getSingleShowtime, buyTicker, getHistory, getShowbyID, getUserInfos, changeProfile, editProfile }