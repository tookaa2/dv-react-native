import { ToastAndroid } from 'react-native'
export const displayMessage = (msg) => {
    ToastAndroid.showWithGravity(
        msg,
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
    );
}
