import React from 'react'
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity, SectionList, TextInput } from 'react-native'
import { buyTicker } from '../utils/dataHelper'
import AutoHeightImage from 'react-native-auto-height-image';
import Swiper from 'react-native-swiper';
import Axios from 'axios';
const { width, height } = Dimensions.get('window')
const frame = require('../assets/frame.png')
import styled from 'styled-components'
const playBtn = require('../assets/play.png')
import PinchZoomView from 'react-native-pinch-zoom-view';
import _ from 'lodash'
import { assembleSeats } from '../actions/SeatActions'
import dateFormat from 'dateformat'
import SeatSets from '../components/SeatSets'
const deluxIcon = require('../assets/Deluxe.png')
const sofaIcon = require('../assets/Sofa.png')
const premiumIcon = require('../assets/Premium.png')
import { Button, InputItem, List } from '@ant-design/react-native';
import Barcode from 'react-native-barcode-builder';


const premiumLabel = ['B', 'C', 'D', 'E']
const deluxeLabel = ['F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N']


class ConfirmPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

        }
        console.log('Confirm page', this.props.location.state)

    }



    componentWillMount() {

    }
    componentDidMount() {
        console.log(this.props)

    }

    _getSeatLabel = (row, seatType) => {

        if (seatType === "SOFA") {
            return "A"
        } else if (seatType === "PREMIUM") {
            return premiumLabel[rowName.replace('row', '')]
        } else {
            return deluxeLabel[rowName.replace('row', '')]
        }
    }
    getSeatSelectedString = () => {

    }

    render() {
        const { movieImage, cinema, date, movieName, price, seats, time, qr } = this.props.location.state
        return (
            <View style={{ flex: 1, backgroundColor: 'lightblue' }}>
                <View style={{ backgroundColor: 'white', elevation: 8, flex: 1, marginHorizontal: 20, marginTop: 40, marginBottom: 300 }}>



                    <View style={{ flex: 2 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <AutoHeightImage source={{ uri: movieImage }} style={{ margin: 10 }} width={100} />
                            <View style={{ flexDirection: 'column', marginTop: 16 }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{movieName}</Text>
                                <Text>{cinema}</Text>
                                <Text>{date}</Text>
                                <Text>{time}</Text>

                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', backgroundColor: 'lightgray', marginHorizontal: -5, elevation: 5 }}>

                            <View style={{ flexDirection: 'column', flex: 1 }}>
                                <Text style={{ fontSize: 20, color: 'black' }}>Total Price</Text>
                                <Text>{price}</Text>
                            </View>
                            <View style={{ flexDirection: 'column', flex: 2 }}>
                                <Text style={{ fontSize: 25, color: 'black' }}>Seats</Text>
                                <Text>{seats}</Text>
                            </View>
                        </View>










                    </View>




                    <View style={{ flex: 1 }}>
                        <Barcode width={1} value={qr || "placement"} format="CODE128" />
                    </View>
                </View>



            </View>

        )
    }
}

const BoldText = styled(Text)`
        font-weight: bold;
    `

const InfoText = styled(Text)`
    line-height:20;
    `
const RowView = styled(View)`
    flex-direction:row
    `

export default ConfirmPage