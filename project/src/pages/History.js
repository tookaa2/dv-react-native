import React from 'react'
import { View, Text, FlatList, TouchableOpacity } from 'react-native'
import { getHistory } from '../utils/dataHelper'
import AutoHeightImage from 'react-native-auto-height-image';
import HistoryContent from '../components/HistoryContent';

class HistoryPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            historyData: []
        }

    }
    componentWillMount() {
        this.fetchData()
    }
    _renderList = ({ item }) => {
        return (
            <HistoryContent style={{ elevation: 5, flexDirection: 'row', backgroundColor: 'white', height: 126, alignSelf: 'stretch', overflow: 'hidden', marginTop: 5 }} history={this.props.history} item={item} />
        )
    }
    fetchData = async () => {
        let historyData = await getHistory(this.props.user.token)
        historyData = historyData.slice(0).reverse().map(value => value)
        this.setState({
            historyData
        })
    }
    state = {

    }
    render() {
        return (
            <View>
                <Text style={{ color: 'black' }}>
                    History Page Header
                </Text>
                <FlatList
                    contentContainerStyle={{ paddingBottom: 100 }}
                    data={this.state.historyData}
                    renderItem={item => this._renderList(item)}
                >

                </FlatList>
            </View>
        )
    }
}


export default HistoryPage