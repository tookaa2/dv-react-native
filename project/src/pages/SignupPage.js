import React from 'react'
import { View, Text, Image, Dimensions } from 'react-native'
import { Button, InputItem, List } from '@ant-design/react-native';
import { register } from '../utils/dataHelper'
const movieBg = require('../assets/MovieTheater.jpg')
import AutoHeightImage from 'react-native-auto-height-image';
const { width } = Dimensions.get('window')

// import styled from 'styled-components'
// import { connect } from 'react-redux'
// import { goBack } from 'connected-react-router'

// import Container from '../components/general/Container'
// import {
//     Body,
//     Button,
//     DefaultText,
//     DefaultInput,
// } from '../components/General.styled'

class SignupPage extends React.Component {
    state = {
        loading: false
    }
    onSignup = async () => {
        this.setState({
            loading: true
        })
        try {
            const result = await register(this.state.email, this.state.firstName, this.state.lastName, this.state.password)
            setTimeout(
                () => {
                    this.props.history.goBack()
                },
                1000
            )
        } catch (error) {
            alert('error')
        }


    }
    onBackPress = () => {
        this.props.history.goBack()
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <AutoHeightImage source={movieBg} blurRadius={5} width={width * 3.5} style={{ position: 'absolute', bottom: 0, top: 0, left: 0, right: 0, alignSelf: 'center' }} />
                <View style={{ marginTop: '20%', marginBottom: '20%' }}>
                    <Text style={{ color: 'white', fontSize: 48, fontFamily: 'serif', alignSelf: 'center' }}>Sign Up</Text>
                </View>
                <View style={{ backgroundColor: 'white', marginHorizontal: 20, padding: 10, }}>
                    <InputItem
                        clear
                        value={this.state.email}
                        onChange={email => {
                            this.setState({
                                email,
                            });
                        }}
                        placeholder="email"
                    />
                    <InputItem
                        clear
                        value={this.state.firstName}
                        onChange={firstName => {
                            this.setState({
                                firstName,
                            });
                        }}
                        placeholder="First name"
                    />
                    <InputItem
                        clear
                        value={this.state.lastName}
                        onChange={lastName => {
                            this.setState({
                                lastName,
                            });
                        }}
                        placeholder="Last name"
                    />
                    <InputItem
                        clear
                        value={this.state.password}
                        onChange={password => {
                            this.setState({
                                password,
                            });
                        }}
                        placeholder="Password"
                    />
                    <InputItem
                        clear
                        value={this.state.password2}
                        onChange={password2 => {
                            this.setState({
                                password2
                            });
                        }}
                        placeholder="Confrim Password"
                    />

                    <Button
                        loading={this.state.loading}
                        onPress={this.onSignup}
                        type="primary"
                    >
                        Sign Up
                    </Button>
                </View>

                <Button
                    loading={this.state.loading}
                    onPress={this.onBackPress}
                    type="primary"
                    style={{ position: 'absolute', top: 0, borderWidth: 0, backgroundColor: null }}
                >
                    <Text style={{ fontSize: 30 }}>
                        X
                </Text>

                </Button>
            </View>
        )
    }
}


export default SignupPage