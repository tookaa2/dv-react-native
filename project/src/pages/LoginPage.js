import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { Button, InputItem, List } from '@ant-design/react-native';
import { ROUTE_MOVIES, ROUTE_SIGNUP } from '../constants/RouteConstants'
import { logIn, getMovies, getUserInfos } from '../utils/dataHelper.js'
const movieBg = require('../assets/MovieTheater.jpg')
class LoginPage extends React.Component {
    constructor(props) {
        super(props)
        console.log('should redirect', this.props.user)
        if (this.props.user.isloggedIn) {
            this.props.history.push('/movies')
        }
    }
    state = {

    }
    UNSAFE_componentWillMount() {
    }
    componentDidMount() {


    }
    onLogin = async () => {
        try {
            const result = await logIn(this.state.username, this.state.password);
            console.log(result)

            const { user } = await getUserInfos(result.token);
            console.log('USER INFO', user)
            this.props.setUser({
                username: result.email,
                firstname: result.firstName,
                lastname: result.lastName,
                token: result.token,
                _id: result._id,
                isloggedIn: true,
                image: user.image
            })
            //TODO REMOVE BOT UNCOM TOP
            // this.props.history.push('/showtime')
            this.props.history.push(ROUTE_MOVIES)
        } catch (error) {
            //TODO ALERT ERROR
            console.log('we got error')
        }


    }
    onSignup = () => {
        console.log(this.props.history)
        this.props.history.push(ROUTE_SIGNUP)
    }
    render() {
        return (
            <View style={{ backgroundColor: 'green', flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Image source={movieBg} resizeMode={'contain'} style={{ position: 'absolute', top: 0, height: '100%' }} />
                <View style={{ width: '80%', backgroundColor: 'white', padding: 10, borderRadius: 5 }}>
                    <InputItem
                        clear
                        value={this.state.username}
                        onChange={username => {
                            this.setState({
                                username
                            });
                        }}
                        placeholder="Username"
                    />
                    <InputItem
                        clear
                        type="password"
                        value={this.state.password}
                        onChange={password => {
                            this.setState({
                                password
                            });
                        }}
                        placeholder="Password"
                    />
                    <Button
                        onPress={this.onLogin}
                        type="primary"
                    >
                        Login
            </Button>
                    <View style={{ flexDirection: 'row' }}>
                        <Text >Don't have an account?</Text>
                        <TouchableOpacity onPress={this.onSignup} style={{ right: 0 }}><Text style={{ fontWeight: 'bold' }}>Sign Up</Text></TouchableOpacity>
                    </View>

                </View>

            </View>
        )
    }
}


export default LoginPage