import React from 'react'
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity, SectionList, TextInput } from 'react-native'
import { buyTicker } from '../utils/dataHelper'
import AutoHeightImage from 'react-native-auto-height-image';
import { Icon } from '@ant-design/react-native'
import Swiper from 'react-native-swiper';
import Axios from 'axios';
const { width, height } = Dimensions.get('window')
const frame = require('../assets/frame.png')
import styled from 'styled-components'
const playBtn = require('../assets/play.png')
import PinchZoomView from 'react-native-pinch-zoom-view';
import _ from 'lodash'
import { assembleSeats } from '../actions/SeatActions'
import dateFormat from 'dateformat'
import SeatSets from '../components/SeatSets'
const deluxIcon = require('../assets/Deluxe.png')
const sofaIcon = require('../assets/Sofa.png')
const premiumIcon = require('../assets/Premium.png')
const premiumLabel = ['B', 'C', 'D', 'E']
const deluxeLabel = ['F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N']


class SeatSelectPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

        }
        console.log('Seat page', this.props.location.state)

    }



    componentWillMount() {
        this.props.setSeats(assembleSeats(this.props.location.state.details.seats))

    }
    componentDidMount() {
        console.log(this.props)

    }

    onBuy = async () => {
        const { movie } = this.props.location.state.details
        const { seat, user } = this.props
        const { cinema, endDateTime, soundtrack, startDateTime, subtitle } = this.props.location.state.details
        try {
            console.log("BUY TICK");

            const result = await buyTicker(this.props.location.state.details._id, seat.selection, user.token)



            this.props.history.push('/confirm', { seats: 'MOCK seats', cinema: cinema.name, time: dateFormat(startDateTime, "hh:MM"), price: this.calTotalPrice(this.props.seat), date: dateFormat(startDateTime, "dd mmm yyyy"), movieImage: movie.image, movieName: movie.name, qr: result.seats[0]._id })
        } catch{
            alert('there is error')
        }


    }
    _getSeatLabel = (row, seatType) => {

        if (seatType === "SOFA") {
            return "A"
        } else if (seatType === "PREMIUM") {
            return premiumLabel[rowName.replace('row', '')]
        } else {
            return deluxeLabel[rowName.replace('row', '')]
        }
    }
    getSeatSelectedString = () => {

    }
    calTotalPrice = (input) => {
        console.log(input.selection)
        let sum = 0;
        input.selection.map(obj => {
            switch (obj.type) {
                case 'DELUXE':
                    sum += 150
                    break;
                case 'Premium':
                    sum += 190
                    break;
                case 'SOFA':
                    sum += 500
                    break;

                default:
                    break;
            }
        })
        return sum
    }
    render() {
        const { cinema, endDateTime, movie, soundtrack, startDateTime, subtitle } = this.props.location.state.details
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{ flexDirection: 'row', borderWidth: 1, borderColor: 'lightblue' }}>
                    <View>
                        <AutoHeightImage source={{ uri: movie.image }} width={80} />
                    </View>
                    <View style={{ flexDirection: 'column' }}>
                        <Text>{movie.name}</Text>
                        <Text>{dateFormat(startDateTime, "dd mmm yyyy")} | {dateFormat(startDateTime, "hh:MM")}</Text>
                        <Text>{cinema.name} | {_.upperCase(soundtrack)}</Text>
                        <Text>SoundInfo</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', height: 70, alignSelf: 'stretch', backgroundColor: 'white' }}>
                    <View style={{ flexDirection: 'column', justifyContent: 'flex-end', flex: 3, alignItems: 'center', marginBottom: 2 }}>
                        <AutoHeightImage source={sofaIcon} width={40} />
                        <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Sofa</Text>
                        <Text style={{ fontSize: 12 }}>฿500</Text>
                    </View>
                    <View style={{ flexDirection: 'column', justifyContent: 'flex-end', flex: 2, alignItems: 'center', marginBottom: 2 }}>
                        <AutoHeightImage source={premiumIcon} width={20} />
                        <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Premium</Text>
                        <Text style={{ fontSize: 12 }}>฿190</Text>
                    </View>
                    <View style={{
                        flexDirection: 'column', justifyContent: 'flex-end', flex: 3, alignItems: 'center', marginBottom: 2
                    }}>
                        < AutoHeightImage source={deluxIcon} width={20} />
                        <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Deluxe</Text>
                        <Text style={{ fontSize: 12 }}>฿150</Text>
                    </View>
                </View>


                <View style={{ overflow: 'hidden', width: width, height: height * 0.5 }}>
                    <PinchZoomView scalable={true} minScale={1} maxScale={5} style={{ backgroundColor: 'white' }}>
                        <View style={{ width: width, height: height * 0.5, alignItems: 'center' }}>
                            <View style={{ backgroundColor: 'orange', width: width }}><Text>Screen</Text></View>
                            <SeatSets seatData={this.props.seat.DELUXE} seatType={"DELUXE"} seatIcon={deluxIcon} />
                            <SeatSets seatData={this.props.seat.PREMIUM} seatType={"PREMIUM"} seatIcon={premiumIcon} />
                            <SeatSets seatData={this.props.seat.SOFA} seatType={"SOFA"} seatIcon={sofaIcon} />
                        </View>

                    </PinchZoomView>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'column', flex: 1, paddingLeft: 20 }}>
                        <Text >Total price</Text>
                        <Text>{this.calTotalPrice(this.props.seat)}</Text>
                    </View>
                    <View style={{ flexDirection: 'column', flex: 1 }}>
                        <Text>Seat Selected</Text>
                        <Text>{this.getSeatSelectedString()}</Text>
                    </View>

                </View>
                <TouchableOpacity onPress={this.onBuy} style={{ backgroundColor: 'white', height: height * 0.08, position: 'absolute', bottom: height * 0.078, width: width }}>
                    <Text>Buy now</Text>
                </TouchableOpacity>

            </View>

        )
    }
}

const BoldText = styled(Text)`
    font-weight: bold;
`

const InfoText = styled(Text)`
line-height:20;
`
const RowView = styled(View)`
flex-direction:row
`

export default SeatSelectPage