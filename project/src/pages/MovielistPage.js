import React from 'react'
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity } from 'react-native'
import { getMovies } from '../utils/dataHelper'
import AutoHeightImage from 'react-native-auto-height-image';
import Swiper from 'react-native-swiper';
import { ROUTE_MOVIEDETAIL, ROUTE_SHOWTIME } from '../constants/RouteConstants'
import { movieExtraDetails } from '../constants/MovieConstants'
const { width } = Dimensions.get('window')
const frame = require('../assets/frame.png')
const promo1= require('../assets/topbar/promo1.jpg')
const promo2= require('../assets/topbar/promo2.png')
const promo3= require('../assets/topbar/promo3.png')
class MovielistPage extends React.Component {
    constructor(props) {
        super(props)
        this.loadMovie()
    }
    _keyExtractor = (item, index) => item._id

    onPress = (input) => {
        // this.props.history.push(ROUTE_MOVIEDETAIL, { item: input })
        this.props.history.push(ROUTE_SHOWTIME, { item: input })
    }
    loadMovie = async () => {
        try {
            const snapshot = await getMovies();
            const Movies = snapshot.data;
            const NewMovies = Movies.map((value, key) => {
                return {
                    ...value,
                    youtubeKey: movieExtraDetails[value._id].youtubeID,
                    engName: movieExtraDetails[value._id].engName,
                }
            })
            console.log(NewMovies)
            this.props.movieAdd(NewMovies);

        } catch (error) {
            console.log(error)
        }
    }
    UNSAFE_componentWillMount() {

    }
    state = {

    }
    renderHeader = () => {
        return (
            <View style={{ height: 198 }}>
                <Swiper autoplay={true} loop={true} width={width} height={150} style={{ arrangeSelf: 'stretch', height: 150 }} showsButtons={true}>
                    <View key="ads1" style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#9DD6ED',
                    }}>
                        <AutoHeightImage source={promo1} width={width}/>
                    </View>
                    <View key="ads2" style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#9DD6EB',
                    }}>
                        <AutoHeightImage source={promo2} width={width}/>
                    </View>
                    <View key="ads3" style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#9DD6EB',
                    }}>
                         <AutoHeightImage source={promo3} width={width}/>
                    </View>
                </Swiper>

            </View>
        );
    };
    _renderMovies = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => { this.onPress(item) }} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <AutoHeightImage
                        width={(width / 2) - 38}
                        source={{ uri: `${item.image}` }}
                        style={{ position: 'absolute', left: 6.5 }}
                    />
                    <AutoHeightImage
                        width={(width / 2) - 25}
                        source={frame}

                    />
                </View>
                <View style={{ marginTop: 5, marginBottom: 5 }}>
                    <Text numberOfLines={1}>{item.name}</Text>
                </View>


            </TouchableOpacity >
        );
    }
    render() {
        return (
            <View >


                <FlatList
                    containerStyle={{ flex: 1 }}
                    contentContainerStyle={{ paddingBottom: 100 }}
                    ListHeaderComponent={this.renderHeader()}
                    numColumns={2}
                    data={this.props.movie.movieList}
                    renderItem={item => this._renderMovies(item)}
                    keyExtractor={this._keyExtractor}
                />


            </View>
        )
    }
}



export default MovielistPage