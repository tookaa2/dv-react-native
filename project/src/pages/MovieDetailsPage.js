import React from 'react'
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity } from 'react-native'
import { getMovies, getDetails } from '../utils/dataHelper'
import AutoHeightImage from 'react-native-auto-height-image';
import { Icon } from '@ant-design/react-native'
import Swiper from 'react-native-swiper';
import Axios from 'axios';
const { width } = Dimensions.get('window')
const frame = require('../assets/frame.png')
import styled from 'styled-components'
const playBtn = require('../assets/play.png')
import YouTube from 'react-native-youtube'
import { YouTubeStandaloneAndroid } from 'react-native-youtube';
class MovieDetailsPage extends React.Component {
    constructor(props) {
        super(props)



        this.state = {
            details: {}
        }

    }

    loadDetails = async () => {
        const response = await getDetails(this.props.history.location.state.item.engName);
        console.log(response.data);
        this.setState({ details: response.data })
    }
    componentWillMount() {
        this.loadDetails()
    }
    componentDidMount() {
        console.log('state', this.state)
    }
    state = {
        isPlay: false
    }
    onPlay = (vidKey) => {
        YouTubeStandaloneAndroid.playVideo({
            apiKey: 'AIzaSyDnGuDSvvE9BqA9ek7DRI1GmF_LqP0lQZU',     // Your YouTube Developer API Key
            videoId: vidKey,     // YouTube video ID
            autoplay: true,             // Autoplay the video
            startTime: 0,             // Starting point of video (in seconds)
        })
            .then(() => console.log('Standalone Player Exited'))
            .catch(errorMessage => console.error(errorMessage))
    }

    render() {
        const { item } = this.props.history.location.state
        return (
            <View >
                <View>
                    <AutoHeightImage source={{ uri: `https://i1.ytimg.com/vi/${item.youtubeKey}/maxresdefault.jpg` }}
                        width={width}
                    />
                    <TouchableOpacity onPress={() => { this.onPlay(item.youtubeKey) }} style={{ opacity: this.state.isPlay ? 0 : 1, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }}>
                        <AutoHeightImage source={playBtn}
                            width={width / 8}
                        />
                    </TouchableOpacity>


                </View>



                <View style={{ position: 'absolute', left: 10, top: 175, backgroundColor: 'white', elevation: 4 }}>
                    <AutoHeightImage
                        width={(width / 4)}
                        source={{ uri: `${item.image}` }}
                    />
                </View>
                <View style={{ height: 110, marginLeft: 110, paddingLeft: 10 }}>
                    <BoldText style={{ lineHeight: 30 }}>{this.state.details.Title ? this.state.details.Title : 'no DATA'}</BoldText>
                    <InfoText >Release Date:    {this.state.details.Released ? this.state.details.Released : 'no DATA'}</InfoText>
                    <InfoText >Rated:    {this.state.details.Rated ? this.state.details.Rated : 'no DATA'} | {this.state.details.Runtime ? this.state.details.Runtime : 'no DATA'}</InfoText>
                </View>
                <View style={{ paddingHorizontal: 20 }}>
                    <RowView >
                        <BoldText>Director:</BoldText>
                        <InfoText> {this.state.details.Director ? this.state.details.Director : 'no data'}</InfoText>
                    </RowView>
                    <RowView >
                        <BoldText>Actors:</BoldText>
                        <InfoText> {this.state.details.Actors ? this.state.details.Actors : 'no data'}</InfoText>
                    </RowView>
                    <RowView >
                        <BoldText>Synopsis: </BoldText>
                    </RowView>
                    <InfoText> {this.state.details.Plot ? this.state.details.Plot : 'no data'}</InfoText>

                </View>



            </View>
        )
    }
}

const BoldText = styled(Text)`
    font-weight: bold;
`

const InfoText = styled(Text)`
line-height:20;
`
const RowView = styled(View)`
flex-direction:row
`

export default MovieDetailsPage