import React from 'react'
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity, SectionList } from 'react-native'
import { getSingleShowtime } from '../utils/dataHelper'
import { Button, InputItem, List } from '@ant-design/react-native';
import AutoHeightImage from 'react-native-auto-height-image';
import { Icon } from '@ant-design/react-native'
import Swiper from 'react-native-swiper';
import Axios from 'axios';
const { width } = Dimensions.get('window')
import styled from 'styled-components'
import _ from 'lodash'
import { ROUTE_SEATSELECT } from '../constants/RouteConstants'
import ShowTabView from '../components/ShowTabView'
class ShowtimePage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showtime: [{
                title: 'Title', data: [{
                    cinema: {
                        name: 'Cinema Name',
                        seats: []
                    }
                }]
            }]
        }
        console.log('SHOW TIME', this.props.location.state.item._id);


    }
    onInfo = () => {
        this.props.history.push('/details', { item: this.props.location.state.item })
    }
    parseByCinema = (dataObj) => {
        let head = [];
        let output = [];

        _.forEach(dataObj, (value, key) => {
            if (!_.includes(head, value.soundtrack)) {
                head.push(value.soundtrack)
            }
        })
        _.forEach(head, (headVal) => {
            let toPush = [];
            _.forEach(dataObj, (value) => {
                if (_.isEqual(value.soundtrack, headVal)) {
                    toPush.push(value);
                }
            })
            output.push({
                title: headVal,
                data: _.cloneDeep(toPush)
            })
        })
        return output;

    }
    loadDetails = async () => {
        const response = await getSingleShowtime(this.props.location.state.item._id, new Date());
        console.log('loading details')
        console.log(response.data);
        const sorted = _.sortBy(response.data, movie => {
            return movie.startDateTime
        })
        console.log(sorted)

        this.setState({ showtime: this.parseByCinema(sorted) })
    }
    _renderShowtime = ({ item }) => {
        var date = new Date(item.startDateTime);
        return (
            <TouchableOpacity onPress={() => {
                this.props.history.push(ROUTE_SEATSELECT, { details: item })
            }} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text>{date.toString()}</Text>
                </View>



            </TouchableOpacity >
        );
    }
    componentWillMount() {
        this.loadDetails()
    }
    componentDidMount() {

        console.log('state', this.state)
    }
    render() {
        return (
            <View style={{ flex: 1 }} >
                <Text>CHIANG MAI CINEMA MOVIE PART </Text>
                <Button
                    loading={this.state.loading}
                    onPress={this.onInfo}
                    type="default"
                >
                    Movie Info
                    </Button>
                <ShowTabView movieId={this.props.location.state.item._id} item={this.props.location.state.item} />






            </View>
        )
    }
}

const BoldText = styled(Text)`
    font-weight: bold;
`

const InfoText = styled(Text)`
line-height:20;
`
const RowView = styled(View)`
flex-direction:row
`

export default ShowtimePage