import React from 'react'
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity, SectionList, TextInput } from 'react-native'
import { getSingleShowtime } from '../utils/dataHelper'
import AutoHeightImage from 'react-native-auto-height-image';
import { Icon } from '@ant-design/react-native'
import Swiper from 'react-native-swiper';
import Axios from 'axios';
const { width } = Dimensions.get('window')
const frame = require('../assets/frame.png')
import styled from 'styled-components'
const playBtn = require('../assets/play.png')
import PinchZoomView from 'react-native-pinch-zoom-view';
import _ from 'lodash'
const sofaIcon = require('../assets/Sofa.png')
const deluxeIcon = require('../assets/Deluxe.png')
const premiumIcon = require('../assets/Premium.png')
class SeatSelectPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            book: {
                'DELUXE': {
                    row1: {

                    }

                }


            }
        }
        console.log('Seat page', this.props.location.state.details.seats)

    }


    componentWillMount() {


    }
    componentDidMount() {


    }
    _renderSeat = (seats) => {
        const reverseSeat = seats.slice(0).reverse().map(value => value)

        return reverseSeat.map((seatType, label) => {
            console.log(seatType)
            return (<View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                {
                    this.renderEachType(seatType)

                }
            </View>);
        });

    }

    renderEachType = (seatType) => {

        return Object.keys(seatType).map((attribute, index) => {
            if (_.startsWith(attribute, 'row') && !_.isEqual('rows', attribute)) {
                console.log('to render: ', seatType[attribute])
                return (<View style={{ flexDirection: 'row' }}>
                    {
                        this.renderEachRow(seatType[attribute], _.replace(attribute, 'row', ''), seatType['type'])
                    }
                </View>)
            }
        });

    }
    renderEachRow = (seatRow, row, type) => {
        return seatRow.map((value, key) => {

            return (<TouchableOpacity onPress={() => {
                console.log('row pressed', row)
                console.log('type pressed', type)
                console.log('column pressed', key + 1)

                this.setState({
                    book: {
                        ...this.state.book,
                        [type]: {
                            [row]: true


                        }
                    }
                })
            }}>
                {_.isEqual(type, 'SOFA') ?
                    <AutoHeightImage width={30} source={sofaIcon} style={{ margin: 5 }} /> :
                    _.isEqual(type, 'DELUXE') ? <AutoHeightImage width={15} source={deluxeIcon} style={{ margin: 2 }} /> :
                        <AutoHeightImage width={15} source={premiumIcon} style={{ margin: 1 }} />

                }{  
                        //TODO tick mark using redux
                    // (!_.isEmpty(this.state.book[type][row][key + 1])) ?
                    //     (this.state.book[type][row][key + 1] ? <Text>12312132</Text> : null) : null
                }

            </TouchableOpacity>)
        })
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'blue' }}>
                <Text>Seat SELECTION</Text>
                <View style={{ overflow: 'hidden', width: width, height: 300 }}>
                    <PinchZoomView scalable={true} minScale={1} maxScale={5} style={{ backgroundColor: 'green' }}>
                        <View style={{ width: width, height: 300, borderWidth: 5, borderColor: 'black' }}>
                            <View style={{ backgroundColor: 'white' }}><Text>Screen</Text></View>
                            {this._renderSeat(this.props.location.state.details.seats)}

                        </View>

                    </PinchZoomView>
                </View>

            </View>

        )
    }
}

const BoldText = styled(Text)`
    font-weight: bold;
`

const InfoText = styled(Text)`
line-height:20;
`
const RowView = styled(View)`
flex-direction:row
`

export default SeatSelectPage