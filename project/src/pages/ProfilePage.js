import React from 'react'
import { View, Text, Dimensions, TouchableOpacity } from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image';
import { Button, InputItem, List } from '@ant-design/react-native';
import _ from 'lodash';
import ImagePicker from 'react-native-image-picker';
const profile = require('../assets/profilePH.jpg')
const bg = require('../assets/MovieTheater.jpg')
const edit = require('../assets/profile/edit.png')
const logout = require('../assets/profile/logout.png')
const ticket = require('../assets/profile/ticket.png')
const { width, height } = Dimensions.get('window')
import { changeProfile } from '../utils/dataHelper'
import { displayMessage } from '../utils/toasterHelper'
const options = {
    title: 'Select Profile Picture',
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};
class ProfilePage extends React.Component {
    constructor(props) {
        super(props)
        console.log('GOT INFO PROFILE', this.props.user)

    }

    state = {

    }

    onChangeImage = async () => {
        ImagePicker.showImagePicker(options, async (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                displayMessage('Cancelled image selection');

            } else if (response.error) {
                displayMessage('ImagePicker Error: ', response.error);

            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let formData = new FormData()
                formData.append('image', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type
                })
                try {
                    const result = await changeProfile(formData, this.props.user.token)
                    this.props.setImage(result.image);
                } catch (error) {
                    alert('image upload error')
                    displayMessage('Image Upload error, please check your internet');
                }
            }
        });
    }
    render() {
        const { firstname, lastname, isloggedIn, image } = this.props.user;
        return (
            <View>
                <AutoHeightImage source={_.isEmpty(image) ? profile : { uri: `${image}` }} width={width * 2} style={{ position: 'absolute', top: 0,opacity:0.9 }} blurRadius={20} />
                <View style={{ justifyContent: 'center', alignItems: 'center', height: height / 2.6, overflow: 'hidden' }}>

                    <AutoHeightImage source={bg} width={width * 2} style={{ position: 'absolute', top: 0,opacity:0 }} blurRadius={10} />

                    <View style={{ justifyContent: 'center', position: 'absolute', top: '15%' }}>
                        <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>Welcome {firstname} {lastname}</Text>
                    </View>

                </View>
                <Button
                    style={{ right: width / 7, top: '25%', width: 50, height: 50, position: 'absolute', borderRadius: 30, marginTop: -20, elevation: 5, zIndex: 10, backgroundColor: 'white', borderColor: 'white' }}
                    onPress={this.onChangeImage}
                    type="default"
                >
                    <Text style={{ color: 'blue' }}>
                        +
                    </Text>

                </Button>
                <View style={{ borderRadius: 20, width: width / 1.5, height: width / 1.5, overflow: 'hidden', position: 'absolute', top: '16%', elevation: 5, alignSelf: 'center' }}>
                    <AutoHeightImage source={_.isEmpty(image) ? profile : { uri: `${image}` }} width={width / 1.5} />
                </View>
                <View style={{ marginTop: 100 }}>

                    <TouchableOpacity onPress={() => {
                        this.props.history.push('/edit', { token: this.props.user.token })
                    }}
                        style={{ height: height / 8, marginTop: 10 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', elevation: 5, marginLeft: 50, backgroundColor: 'white',opacity:0.55, height: '100%', borderTopLeftRadius: 500, borderBottomLeftRadius: 500 }}>
                            <AutoHeightImage source={edit} width={56} style={{ marginLeft: 30 }} />
                            <Text style={{ color: 'gray', fontSize: 28, marginLeft: 20 }}>
                                Edit Profile
                            </Text>
                        </View>

                    </TouchableOpacity>
                    <TouchableOpacity style={{ height: height / 8, marginTop: 10 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', elevation: 5, marginRight: 50, backgroundColor: 'white',opacity:0.55, height: '100%', borderTopRightRadius: 500, borderBottomRightRadius: 500 }}>
                         
                            <Text style={{ color: 'gray', fontSize: 28 ,marginLeft:20}}>
                                Tickets History
                            </Text>
                            <AutoHeightImage source={ticket} width={56} style={{ marginLeft: 30 }} />
                        </View>

                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        this.props.clearUser()

                        ///Not popToTop for this library so this is popTo Top
                        this.props.history.go(-100)
                    }}
                        style={{ height: height / 8, marginTop: 10 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', elevation: 5, marginLeft: 50,backgroundColor:'orange', borderColor:'white',borderLeftWidth:2,borderTopWidth:2,borderBottomWidth:2, height: '100%', borderTopLeftRadius: 500, borderBottomLeftRadius: 500 }}>
                            <AutoHeightImage source={logout} width={56} style={{ marginLeft: 30 }} />
                            <Text style={{ color: 'white', fontSize: 28, marginLeft: 20 }}>
                                Logout
                            </Text>
                        </View>

                    </TouchableOpacity>

                </View>



            </View >
        )
    }
}


export default ProfilePage