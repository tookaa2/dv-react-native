import React from 'react'
import { View, Text, FlatList, TouchableOpacity, Dimensions } from 'react-native'
import { getHistory } from '../utils/dataHelper'
import AutoHeightImage from 'react-native-auto-height-image';
import HistoryContent from '../components/HistoryContent';
import {ROUTE_SHOWTIME} from '../constants/RouteConstants' 
import Carousel from 'react-native-snap-carousel';
const blade = require('../assets/promo/blade.jpg')
const { width, height } = Dimensions.get('window')
const fakeData = [
    {
        title: 'Blade Runner',
        img: require('../assets/promo/blade.jpg'),
        imgUrl:'https://images-na.ssl-images-amazon.com/images/I/71PhjEaTZ6L._SL1111_.jpg',
        genre: 'Action,Thriller',
        date: '25 March 2019',
        youtubeID: 'gCcx85zbxz4'


    },
    {
        title: 'Avengers Endgame',
        img: require('../assets/promo/avengers.png'),
        imgUrl:'https://media.comicbook.com/2019/03/avengers-endgame-poster-full-1162928.jpeg',
        genre: 'Action,Sci-Fi',
        date: '5 April 2019',
        youtubeID: 'ee1172yeqyE'
    },

    {
        title: 'A Wrinkle In Time',
        img: require('../assets/promo/disney.jpg'),
        imgUrl:'https://www.easons.com/globalassets/5637150827/all/books/childrens/fiction-teenage/teenage-fiction-12-to-15-years/97802413311632.jpg?width=270&height=360&mode=max',
        genre: 'Fantasy,Drama',
        date: '8 April 2019',
        youtubeID: 'UhZ56rcWwRQ'
    }

]
class PromotionPage extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            movieTitle: fakeData[0].title,
            movieGenre: fakeData[0].genre,
            showDate: fakeData[0].date,
            youtubeKey:fakeData[0].youtubeID,
            imageUrl:fakeData[0].imgUrl
        }
    }
    componentWillMount() {

    }
    _renderItem({ item, index }) {
        return (
            <View style={{ width: width - 80, height: '100%', alignItems: 'center', backgroundColor: 'red' }}>
                <View style={{ width: '100%', borderRadius: 15, overflow: 'hidden', marginTop: '5%', backgroundColor: 'blue' }}>
                    <AutoHeightImage source={item.img} width={width - 80} />
                </View>
                <View style={{ backgroundColor: 'lightgray', width: '80%', borderBottomLeftRadius: 5, borderBottomRightRadius: 5 }}>
                    <Text style={{ color: 'darkgray' }}>{item.title}</Text>
                </View>

            </View>
        );
    }
    state = {

    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <Text style={{ fontSize: 25, alignSelf: 'center' }}>Coming Soon</Text>
                <View style={{ height: '66%', backgroundColor: 'green' }}>
                    <Carousel
                        onSnapToItem={(slideIndex) => {
                            this.setState({
                                movieTitle: fakeData[slideIndex].title,
                                movieGenre: fakeData[slideIndex].genre,
                                showDate: fakeData[slideIndex].date,
                                youtubeKey:fakeData[slideIndex].youtubeID,
                                imageUrl:fakeData[slideIndex].imgUrl,
                            })

                        }}
                        ref={(c) => { this._carousel = c; }}
                        data={fakeData}
                        renderItem={this._renderItem}
                        sliderWidth={width}
                        itemWidth={width - 80}
                    />
                </View>

                <View style={{ top: 0 }}>
                    <View>
                        <Text style={{ fontSize: 36 }}>{this.state.movieTitle}</Text>
                    </View>

                    <Text>Genre: {this.state.movieGenre}</Text>
                    <Text>Show date: {this.state.showDate}</Text>
                    <TouchableOpacity onPress={() => {
                         this.props.history.push('/details', { item: {
                            engName:this.state.movieTitle,
                            youtubeKey:this.state.youtubeKey,
                            image:this.state.imageUrl
                         } })
                     

                    }} style={{ borderColor: 'lightblue', borderWidth: 3, width: 200, borderRadius: 100, alignItems: 'center', alignSelf: 'center' }}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>View Details</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}


export default PromotionPage