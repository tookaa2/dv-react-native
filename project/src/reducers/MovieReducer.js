// import { USER_SAVE } from '../constants/AppConstants'
const DEFAULT_STATE = {
    movieList: [],
    currentSelected: {}
}
export default (state = DEFAULT_STATE, { type, payload }) => {
    switch (type) {
        case 'MOVIE_SET_LIST':
            return {
                ...state,
                movieList: payload.list
            }
        default:
            return state
    }
}
