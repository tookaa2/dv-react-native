// import { USER_SAVE } from '../constants/AppConstants'
import _ from 'lodash'
const DEFAULT_STATE = {
    DELUXE: {},
    SOFA: {},
    PREMIUM: {},
    selection: []
}
export default (state = DEFAULT_STATE, { type, payload }) => {
    switch (type) {
        case 'SET_SEAT':
            return {
                ...state,
                DELUXE: payload.DELUXE,
                SOFA: payload.SOFA,
                PREMIUM: payload.PREMIUM
            }
        case 'ADD_SELECTION':
            return {
                ...state,
                selection: [...state.selection, payload]
            }
        case 'REMOVE_SELECTION':

            return {
                ...state,
                selection: state.selection.filter((item) => {
                    console.log(item)
                    console.log(payload);
                    console.log(_.isEqual(item, payload));



                    return !_.isEqual(item, payload)
                })
            }
        case 'CLEAR_SELECTION':
            return {
                ...state,
                selection: []
            }

        default:
            return state
    }
}
