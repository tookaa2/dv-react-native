// import { USER_SAVE } from '../constants/AppConstants'
const DEFAULT_STATE = {
    username: '',
    firstname: '',
    lastname: '',
    token: '',
    _id: '',
    isloggedIn: false,
    image: ''
}
export default (state = DEFAULT_STATE, { type, payload }) => {
    switch (type) {
        case 'USER_SAVE':
            return { ...state, ...payload }
        case 'USER_IMAGECHANGE':
            return {
                ...state,
                image: payload
            }
        case 'USER_CLEAR':
            return DEFAULT_STATE
        default:
            return state
    }
}
