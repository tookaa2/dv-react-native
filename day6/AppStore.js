import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import logger from 'redux-logger'

import { createMemoryHistory } from 'history'
import { routerMiddleware } from 'connected-react-router'

// import TodosReducer from "./TodosReducer";
import UserReducer from "./reducers/UserReducer";
import ProductReducer from './reducers/ProductReducer'
import { connectRouter } from 'connected-react-router'
const reducers = (history) => combineReducers({
  user: UserReducer,
  product: ProductReducer,
  router: connectRouter(history)
})

export const history = createMemoryHistory();

export const store = createStore(reducers(history), compose(
  applyMiddleware(
    routerMiddleware(history),
    logger),
));
