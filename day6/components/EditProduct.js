import React, { Component } from "react";
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from "react-native";
import { commonStyles } from "../Component1";
import { Button, InputItem, Icon } from "@ant-design/react-native";
import { connect } from "react-redux";
import ImagePicker from 'react-native-image-picker';
const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
type Props = {};
class EditProduct extends Component<Props> {

  constructor(props) {
    super(props);
    const { name, image, id } = this.props.location.state;
    this.state = {
      name,
      image,
      id
    }
  }
  goBack = () => {
    this.props.history.goBack();
  };
  onEdit = () => {
    this.props.editProduct(
      {
        image: this.state.image,
        id: this.state.id,
        name: this.state.name
      }
    )
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={[commonStyles.menuBar, commonStyles.lightBlue]}>
          <TouchableOpacity
            onPress={this.goBack}
            style={[commonStyles.menuSquare, commonStyles.canaryYellow]}
          >
            <Icon style={[commonStyles.buttonText]} name="left" />
          </TouchableOpacity>
          <Text style={commonStyles.buttonText}>Edit PRODUCT</Text>
        </View>
        <ScrollView scrollEnabled={true} style={{ flex: 1 }}>
          <Button
            style={[commonStyles.button, commonStyles.profileButttonMargin]}
            onPress={() => {
              ImagePicker.showImagePicker(options, (response) => {
                console.log('Response = ', response);
                if (response.didCancel) {
                  console.log('User cancelled image picker');
                } else if (response.error) {
                  console.log('ImagePicker Error: ', response.error);
                } else if (response.customButton) {
                  console.log('User tapped custom button: ', response.customButton);
                } else {
                  const source = { uri: response.uri };
                  this.setState({
                    image: source,
                  });
                }
              });
            }}
            type="primary"
          >
            <Text style={commonStyles.buttonText}>Image</Text>
          </Button>
          <InputItem
            onFocus={() => {
              this.setState({ name: "" });
            }}
            value={this.state.name}
            onChange={name => this.setState({ name })}
            extra=""
            placeholder="Name"
          />

        </ScrollView>
        <View style={[commonStyles.footerButton]}>
          <Button
            style={[commonStyles.button]}
            onPress={this.onEdit}
            type="primary"
          >
            <Text style={commonStyles.buttonText}>Save</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    // alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});

const mapStateToProps = state => {
  return {
    user: state.user,
    product: state.product
  };
};
const mapDispatchToProps = dispatch => {
  return {
    editProduct: product => {
      dispatch({
        type: "EDIT_PRODUCT",
        product
      });
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProduct);
