import React, { Component } from "react";
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from "react-native";
import { commonStyles } from "../Component1";
import { Button, InputItem, Icon } from "@ant-design/react-native";
import { connect } from "react-redux";
import ImagePicker from 'react-native-image-picker';
const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
type Props = {};
class AddProduct extends Component<Props> {
  constructor(props) {
    super(props);
    const id = Math.random().toString(36).substr(2, 9);
    this.state = {
      name: '',
      image: null,
      id
    }
    console.log(this.props);
    console.log(this.state)
  }
  goBack = () => {
    this.props.history.goBack();
  };
  onSubmit = () => {
    this.props.addProduct({ image: this.state.image, name: this.state.name, id: this.state.product })
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={[commonStyles.menuBar, commonStyles.lightBlue]}>
          <TouchableOpacity
            onPress={this.goBack}
            style={[commonStyles.menuSquare, commonStyles.canaryYellow]}
          >
            <Icon style={[commonStyles.buttonText]} name="left" />
          </TouchableOpacity>
          <Text style={commonStyles.buttonText}>ADD PRODUCT</Text>
        </View>
        <ScrollView scrollEnabled={true} style={{ flex: 1 }}>
          <Button
            style={[commonStyles.button, commonStyles.profileButttonMargin]}
            onPress={() => {
              ImagePicker.showImagePicker(options, (response) => {
                console.log('Response = ', response);

                if (response.didCancel) {
                  console.log('User cancelled image picker');
                } else if (response.error) {
                  console.log('ImagePicker Error: ', response.error);
                } else if (response.customButton) {
                  console.log('User tapped custom button: ', response.customButton);
                } else {
                  const source = { uri: response.uri };
                  console.log(source)
                  // You can also display the image using data:
                  // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                  this.setState({
                    image: source,
                  });
                }
              });
            }}
            type="primary"
          >
            <Text style={commonStyles.buttonText}>Image</Text>
          </Button>
          <InputItem
            onFocus={() => {
              this.setState({ username: "" });
            }}
            value={this.state.name}
            onChange={name => this.setState({ name })}
            extra=""
            placeholder="Name"
          />
        </ScrollView>
        <View style={[commonStyles.footerButton]}>
          <Button
            style={[commonStyles.button]}
            onPress={this.onSubmit}
            type="primary"
          >
            <Text style={commonStyles.buttonText}>Save</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    // alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});

const mapStateToProps = state => {
  return {
    user: state.user,
    product: state.product
  };
};
const mapDispatchToProps = dispatch => {
  return {
    addProduct: product => {
      dispatch({
        type: "ADD_PRODUCT",
        product
      });
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddProduct);
