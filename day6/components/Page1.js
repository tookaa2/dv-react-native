
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Modal,
  TouchableHighlight,
  Image
} from "react-native";
import { connect } from "react-redux";
import { Link } from "react-router-native";
import { MyButton, StandardButton, commonStyles } from "../Component1";
import styled from "styled-components/native";
import { Button, InputItem, List } from "@ant-design/react-native";
type Props = {};

 class Page1 extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      username: "username",
      password: "password",
      isModalVisible: false,
      modalText: ""
    };
  }
  goToScreen = () => {
    this.props.history.push("/Page2");
  };

  onSubmit = () => {
    this.setState({ isModalVisible: true, modalText: "Correct" });
    this.props.history.push("/Page2", { myusername: this.state.username });
    this.props.setName(this.state.username);
  };
  render() {
    return (
      <View style={{ flex: 1, display: "flex" }}>
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
        <View style={{ width: 200, height: 200, borderRadius: 200,overflow:'hidden' }}>
        <Image
            source={require("../giphy.gif")}
            style={{ width: 200, height: 200, borderRadius: 200 }}
          />
        </View>
         

          <Text>{this.state.username + " - " + this.state.password}</Text>
        </View>
        <View style={{ flex: 1 }}>
          <InputItem
      
            onFocus={() => {
              this.setState({ username: "" });
            }}
            value={this.state.username}
            onChange={username => this.setState({ username })}
            extra=""
            placeholder="Username"
          />

          <InputItem
            onFocus={() => {
              this.setState({ password: "" });
            }}
            value={this.state.password}
            onChange={password => this.setState({ password })}
            extra=""
            placeholder="Password"
          />

          <Button
            style={[commonStyles.button, commonStyles.loginPosition]}
            onPress={() => {
              this.onSubmit();
            }}
            type="primary"
          >
            <Text style={commonStyles.buttonText}>Login</Text>
          </Button>
        </View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.isModalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
          }}
        >
          <View
            style={{
              margin: 50,
              backgroundColor: "white",
              height: 100,
              marginTop: 300,
              borderRadius: 10,
              borderColor: "aqua",
              borderWidth: 2
            }}
          >
            <TouchableHighlight
              onPress={() => {
                this.setState({ isModalVisible: false });
              }}
            >
              <Text style={{ fontSize: 25 }}>X</Text>
            </TouchableHighlight>
            <View style={{ justifyContent: "center" }}>
              <Text style={{ fontSize: 25 }}>{this.state.modalText}</Text>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
const CustonLink = styled(Link)`
  padding-top: 20;
  background-color: pink;
  border-width: 1;
  border-style: solid;
  border-radius: 5;
`;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});

const mapStateToProps = state => {
  return {
    user: state.user
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setName: name => {
      dispatch({
        type: "SET_USERNAME",
        name
      });
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Page1);
