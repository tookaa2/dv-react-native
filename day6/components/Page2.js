import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image
} from "react-native";
import { commonStyles } from "../Component1";
import { connect } from "react-redux";
type Props = {};
const fakeData = [
  { image: require('../cat.jpg'), name: 'Product1' },
  { image: require('../cat.jpg'), name: 'Product2' },
  { image: require('../cat.jpg'), name: 'Product3' },
  { image: require('../cat.jpg'), name: 'Product4' },
  { image: require('../cat.jpg'), name: 'Product5' },
  { image: require('../cat.jpg'), name: 'Product6' },
  { image: require('../cat.jpg'), name: 'Product7' },
  { image: require('../cat.jpg'), name: 'Product8' },
  { image: require('../cat.jpg'), name: 'Product9' },
  { image: require('../cat.jpg'), name: 'Product10' }];
class Page2 extends Component<Props> {
  constructor(props) {
    super(props);
    console.log('second page props', this.props)
  }
  goBack = () => {
    this.props.history.goBack();
  };
  onAddProduct = () => {
    this.props.history.push("/AddProduct", { myusername: "123" });
  };
  goProduct = (item) => {
    console.log('TESTING', item.image)
    this.props.history.push("/Product", { name: item.name, image: item.image, id: item.id });
  };
  onProfile = () => {
    this.props.history.push("/Profile", { myusername: "123" });
  };
  renderItems = (items) => {

    const toRender = [];
    let index = 0;
    items.map((value, key) => {

      if (key % 2 === 0) {
        toRender[index] = {
          ...toRender[index],
          item1: value
        }

      } else {
        toRender[index] = {
          ...toRender[index],
          item2: value
        }
        index++;
      }
    })
    return toRender.map((value, key) => {
      return <View
        style={{
          flexDirection: "row",
          display: "flex",
          height: 150,
          marginTop: 5
        }}
      >
        <TouchableOpacity onPress={() => { this.goProduct(value.item1) }} style={commonStyles.gridObject}>
          <Image
            source={value.item1.image}
            style={{ width: 120, height: 120, borderRadius: 5 }}
          />
          <Text>{value.item1.name}</Text>
        </TouchableOpacity>
        {
          value.item2 ? <TouchableOpacity onPress={() => { this.goProduct(value.item2) }} style={commonStyles.gridObject}>
            <Image
              source={value.item2.image}
              style={{ width: 120, height: 120, borderRadius: 5 }}
            />
            <Text>{value.item2.name}</Text>
          </TouchableOpacity> : <View style={[commonStyles.gridObject, { borderWidth: 0 }]} />
        }

      </View>
    })

  }
  render() {
    return (
      <View style={styles.container}>
        <View style={[commonStyles.menuBar, commonStyles.lightBlue]}>
          <Text>{this.props.location.state.myusername}</Text>
        </View>
        <ScrollView scrollEnabled={true} style={{ flex: 1 }}>
          {this.renderItems(this.props.product.products)}
        </ScrollView>
        <View style={[commonStyles.menuBar, commonStyles.lightBlue]}>
          <TouchableOpacity onPress={this.goBack} style={[commonStyles.menuSquare, commonStyles.canaryYellow]}>
            <Text style={commonStyles.buttonText}>L</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.onAddProduct} style={{ flex: 1 }}>
            <Text style={commonStyles.buttonText}>Add</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={this.onProfile}
            style={[commonStyles.menuSquare, commonStyles.canaryYellow]}
          >
            <Text style={commonStyles.buttonText}>P</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    // alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});

const mapStateToProps = state => {
  return {
    user: state.user,
    product: state.product
  };
};
const mapDispatchToProps = dispatch => {
  return {
    addProduct: product => {
      dispatch({
        type: "ADD_PRODUCT",
        product
      });
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Page2);
