import React, { Component } from "react";
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Image } from "react-native";
import { commonStyles } from "../Component1";
import { Button, InputItem, List, Icon } from "@ant-design/react-native";
type Props = {};
export default class Product extends Component<Props> {
  constructor(props) {

    super(props);
    const { name, image, id } = this.props.location.state;
    this.state = {
      name,
      image,
      id
    }
  }
  goBack = () => {
    this.props.history.goBack();
  };
  EditProduct = () => {
    this.props.history.push("/EditProduct", { name: this.state.name, image: this.state.image, id: this.state.id });
  };
  render() {
    const { name, image } = this.props.location.state;
    return (
      <View style={styles.container}>
        <View style={[commonStyles.menuBar, commonStyles.lightBlue]}>
          <TouchableOpacity
            onPress={this.goBack}
            style={[commonStyles.menuSquare, commonStyles.canaryYellow]}
          >
            <Icon style={[commonStyles.buttonText]} name="left" />
          </TouchableOpacity>
          <Text style={commonStyles.buttonText}>Product</Text>
        </View>
        <ScrollView scrollEnabled={true} style={{ flex: 1 }}>
          <View style={{ height: 150, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Image
              source={image}
              style={{ width: 120, height: 120, borderRadius: 5 }}
            />
          </View>
          <View style={commonStyles.profileItems}>
            <Text style={[commonStyles.buttonText, { alignSelf: 'center' }]}>{name}</Text>
          </View>

        </ScrollView>
        <View style={[commonStyles.footerButton]}>
          <Button
            style={[commonStyles.button]}
            onPress={this.EditProduct}
            type="primary"
          >
            <Text style={commonStyles.buttonText}>Edit</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    // alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
