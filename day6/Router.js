import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Page1 from './components/Page1';
import Page2 from './components/Page2';
import Profile from './components/Profile';
import EditProfile from './components/EditProfile';
import Product from './components/Product';
import EditProduct from './components/EditProduct';
import AddProduct from './components/AddProduct';
import { Provider } from "react-redux";
import { store, history } from "./AppStore";
import { ConnectedRouter } from 'connected-react-router'
export default class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route exact path="/Page1" component={Page1} />
                        <Route exact path="/Page2" component={Page2} />
                        <Route exact path="/Profile" component={Profile} />
                        <Route exact path="/EditProfile" component={EditProfile} />
                        <Route exact path="/Product" component={Product} />
                        <Route exact path="/EditProduct" component={EditProduct} />
                        <Route exact path="/AddProduct" component={AddProduct} />
                        <Redirect to="/Page1"></Redirect>
                    </Switch>
                </ConnectedRouter>
            </Provider>
        )
    }
}